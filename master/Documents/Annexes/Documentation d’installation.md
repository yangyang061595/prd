# Documentation d’installation

### Générateur d’instances, constructeur de base d’apprentissage, mat- heuristiques

Ces outils se nomment respectivement "GenInstanceFlowshop", "GenBDAvecDescripteurs" et "Matheuristic" sur le Git. Ils ont été compilés avec Visual Studio 2017 et les exécutables sont disponibles dans les sous-dossiers "x64/Release" de chaque projet.

Le projet C++ Visual Studio de chaque outil est également fourni. Pour compiler ces différents outils avec les fichiers Visual Studio fournis, il est nécessaire d’installer :

-  Microsoft Visual Studio 2017 (set General->Platform Toolset-> Visual Studio 2015 v140)
- IBM CPLEX 12.10

Puis de compiler en 64 bits et en mode Release.

### Constructeurdemodèledeprédiction

Le constructeur de modèle de prédiction s’exécute dans un environnement Python. Pour l’utili-

ser, il est nécessaire d’installer :

-  Python 3.7.6 (64 bits de préférence, surtout pour apprendre un modèle à partir de grandes bases d’apprentissage)
- Les dépendances Python du projet : elles sont listées dans le fichier requirements.txt à la racine du Git. Pour installer les dépendances, il suffit d’exécuter la commande "pip install -r requirements.txt"

### Documentationdéveloppeurs

Les documentations des fonctions et classes de chaque outil sont disponibles dans le dossier html de chaque projet.