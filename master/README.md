# Génération d'une instance de flowshop

Pour générer x instances de n jobs et m machines :

```shell
GenInstanceFlowshop.exe n m x
```

Les instances sont générées dans le sous-dossier /instances.

# Génération d'une base d'apprentissage avec descripteurs

Pour générer une base d'apprentissage avec descripteurs à partir d'un dossier d'instances :

```shell
GenBDAvecDescripteurs.exe nom_dossier_instances numero_instance_min numero_instance_max proba_cross_validation_base_apprentissage RBS random_seed(optionnel)
```

ou en version multithread :

```shell
GenBDAvecDescripteursMultithread.bat nom_dossier_instances nb_instances nb_threads proba_cross_validation_base_appr RBS
```

La sortie donne un fichier DatabaseTrain.txt et DatabaseTest.txt qui contiennent respectivement la base d'apprentissage et la base de test. Les deux bases ne contiennent pas d'instances communes.

Il est donc possible de lancer plusieurs fois le programme ```GenBDAvecDescripteurs.exe``` afin de paralléliser la génération de la base d'apprentissage.
Exemple pour paralléliser le calcul de 20 instances sur 4 coeurs sans RBS(rbs = 0) :

```shell
GenBDAvecDescripteurs.exe instances 1 5 0.5 0
GenBDAvecDescripteurs.exe instances 6 10 0.5 0
GenBDAvecDescripteurs.exe instances 11 15 0.5 0
GenBDAvecDescripteurs.exe instances 16 20 0.5 0
```

Ou directement avec le script multithread :

```shell
GenBDAvecDescripteursMultithread.bat instances 20 4 0.5 0
```

# Construction d'un modèle de prédiction

Exécuter

```shell
python GenModPredictionPourcentImp.py chemin_base_apprentissage chemin_base_test silent_mode
```

Ce script construit le modèle de prédiction avec la méthode LASSO et affiche différentes statistiques sur la construction de celui-ci (LASSO Path, Nombre de variables en fonction de alpha, MSE en fonction de alpha). 

silent_mode : 
- "graphs", affiche les différents graphiques au cours de la construction du modèle
- "silent", permet de lancer la construction du modèle en silence (sans les graphiques)

Le modèle est ensuite exporté dans un fichier model.dat

# Utilisation du modèle de prédiction dans la matheuristique

Pour lancer la résolution d'un problème de flowshop à l'aide du prédicteur :

```shell
Matheuristic.exe chemin_fichier_instance chemin_fichier_modele numero_matheuristique limite_temps(pour matheuristique 1 et 2) RBS random_seed(optionnel)
```

numero_matheuristique peut prendre différentes valeurs :
- 1 : bruteforce avec prédicteur (tous les r, 4<=h<=16)
- 2 : matheuristique originale avec prédicteur (tous les r, h=12)
- 3 : nouvelle matheuristique (deltas décroissant, stoppe à 70% à la liste des delts) avec prédicteur (tous les r, 4<=h<=12)
- 4 : nouvelle matheuristique (deltas décroissant, stoppe à 10% du pire delta) avec prédicteur (tous les r, 4<=h<=12)
- 5:matheuristique originale sans prédicteur (tous les r, h=12)
- 6: best prediction matheuristique (tire au hasard r , le meilleure prédiction parmi 4<= h <=12) avec predictor .
- 7: random matheuristic without predictor (tire au hasard r,  4<= h <=12 )