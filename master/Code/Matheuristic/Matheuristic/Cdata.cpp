#include "CData.h"

vector<Job> N;

/**
 * Reads the instance from a given filepath.
 *
 * \param fname the path to the instance file.
 * \param N the jobs vector to fill.
 */
void read_jobs(const char *fname, vector<Job>& N)
{
	int dummy;
	N.resize(0);
	ifstream ifs(fname);
	if (ifs.is_open()) {
		Job buf;
		while (!ifs.eof()) {
			ifs >> buf.id;
			buf.id--;
			ifs >> buf.ptime[0] >> buf.ptime[1];
			if (!ifs.eof())
				N.push_back(buf);
		}
	}
	else {
		char msg[128];
		snprintf(msg, 127, "read_jobs(): cannot open %s", fname);
		throw(msg);
	}
}
