#include "CModelPrediction.h"

/**
 * Constructs a CModelPrediction from a given path. 
 *
 * \param modelPath the path to the model file.
 */
CModelPrediction::CModelPrediction(string sModelPath, bool useFDEEP)
{
	this->useFDEEP = useFDEEP;
	if (useFDEEP) {
		// use fdeep to load model
		pModel = std::make_unique<fdeep::model>(fdeep::load_model(sModelPath));
	}
	else {
		// Parsing coefficients for each feature
		ifstream modelFile(sModelPath);
		string sLine;
		int iIteration = 0;
		bool varianceParsed = false;
		bool meanParsed = false;
		while (getline(modelFile, sLine))
		{
			if (iIteration != 0) {
				istringstream iss(sLine);
				vector<string> results(istream_iterator<string>{iss}, istream_iterator<string>());
				if (results.size() == 5) {
					cout << results[1] << " " << results[2] << endl;
					MODELCoefficientsFeatures[results[1]] = stod(results[2]);
					MODELVariancesFeatures[results[1]] = stod(results[3]);
					MODELMeansFeatures[results[1]] = stod(results[4]);
				}
				else if (!meanParsed) {
					MODELMean = stod(sLine);
					meanParsed = true;
				}
				else if (!varianceParsed) {
					MODELVariance = stod(sLine);
					varianceParsed = true;
				}
				else {
					break;
				}
			}
			iIteration++;
		}
	}
	
}

/**
 * Predicts the delta from a given list of features.
 *
 * \param modelPath {key,value} structure for storing computed features values.
 * \return delta the predicted delta.
 */
double CModelPrediction::MODELPredict(map<string, double> features)
{
	if (useFDEEP) {
		std::vector<double> v;
		for (auto const& feature : features)
		{
			v.push_back(feature.second);
		}
		const fdeep::tensor input(fdeep::tensor_shape(83), v);
		const auto prediction = pModel->predict_single_output({ input });
		return prediction;
	}
	else {
		double dPredictValue = 0;
		for (map<string, double>::iterator iter = features.begin(); iter != features.end(); ++iter)
		{
			if (MODELCoefficientsFeatures.find(iter->first) != MODELCoefficientsFeatures.end()) {
				if (MODELVariancesFeatures[iter->first] != 0) {
					double dStandardizedFeature = (iter->second - MODELMeansFeatures[iter->first]) / sqrt(MODELVariancesFeatures[iter->first]);
					dPredictValue += MODELCoefficientsFeatures[iter->first] * dStandardizedFeature;
					//cout << iter->first << "," << MODELVariancesFeatures[iter->first] << "," << MODELCoefficientsFeatures[iter->first] << " | " << dPredictValue << endl;
				}
			}
			else {
				//cout << "WARNING : the coefficient for feature " << iter->first << " is not present in the prediction model. Its value will not be used for prediction." << endl;
			}
		}
		//system("pause");
		//cout << endl;
		return dPredictValue*sqrt(MODELVariance) + MODELMean;
	}
	
}

/**
 * Destructor.
 */
CModelPrediction::~CModelPrediction()
{
}
