using namespace std;

#include <ilcplex/ilocplex.h>
ILOSTLBEGIN

#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <stdio.h>
#include <process.h>
#include "CData.h"
#include "CModelPrediction.h"
#include "Cstatistics.h"
#include "CCouple.h"

#define MAX_JOBS 600
#define MAX_ITER 500

double sp_time;
double avg_sp_time = 0;
double max_sp_time = 0;

int initial_obj;

int sp_cnt = 0;

/**
 * Gets sequence from CPlex result
 * \param cpx the CPlex instance
 * \param x the list of constraints
 * \param N the problem instance
 * \return S the sequence corresponding to the CPlex result
 */
void get_sequence(IloCplex cpx, IloIntVarArray x[], const vector<Job>& N, vector<int>& S)
{
	for (int i = 0; i < N.size(); ++i) {
		for (int j = 0; j < N.size(); ++j) {
			if (cpx.getValue(x[i][j]) > .5) {
				S[j] = N[i].id;
			}
		}
	}
}

/**
 * New matheuristic hybrided with the predictor. Sort all deltas in descending order. Iterates through all R values and 4<=H<=12. Stops at the 10% worst delta.
 *
 * \param cpx the CPlex instance
 * \param x the list of constraints
 * \param N the problem instance
 * \param S the sequence to optimize
 * \param bestobj the objective value returned by the RBS heuristic
 * \param model the predictor
 * \param timelim the time limit
 */
bool searchMetaHybrid10Percent(IloCplex cpx, IloIntVarArray x[], vector<Job>& N, vector<int>& S, int&bestobj, CModelPrediction& model)
{
	unsigned int wsize;
	unsigned int wstart;
	unsigned int uiIter = 0;
	unsigned int uiLoop;
	FILE *fichier;
	vector<int> bestS(N.size());
	unsigned int uiBestr = 0, uiBesth = 0;
	unsigned int uiObjInit = bestobj, bestobjiter;
	double dPerImpInit, dPerImp;
	bool bImproved;
	CStatistics statistics;
	vector<CCouple> predictedDeltas;

	cout << "initial=" << bestobj << endl;
	for (uiLoop = 0; uiLoop < N.size(); uiLoop++)
		bestS[uiLoop] = S[uiLoop];

	clock_t start_time = clock();

	bImproved = true;
	printf("Starting search\n");


	//While solution is improved (we're not in a local minimum)
	while (bImproved) {
		printf("Iteration %d\n", uiIter + 1);
		bImproved = false;
		bestobjiter = bestobj;
		predictedDeltas.clear();

		//Test every window size from h=4 to h=12
		for (wsize = 4; wsize <= 12; wsize++)
		{
			//Test every possible window start position
			for (wstart = 0; wstart < N.size() - wsize; wstart++)
			{
				//Compute statistics
				statistics.STATComputeStatistics(&S, S.size(), wstart, wsize);
				map<string, double> features = statistics.STATGetStatistics();

				double prediction = model.MODELPredict(features);

				CCouple couple(wstart, wsize, prediction);
				predictedDeltas.push_back(couple);
			}
		}

		sort(predictedDeltas.begin(), predictedDeltas.end());
		reverse(predictedDeltas.begin(), predictedDeltas.end());

		double dLimit = predictedDeltas.back().COUPLEGetCouplePrediction() / (double)10;
		unsigned int indice = 0;
		while (bImproved == false && predictedDeltas[indice].COUPLEGetCouplePrediction() > dLimit) {

			wstart = predictedDeltas[indice].COUPLEGetR();
			wsize = predictedDeltas[indice].COUPLEGetH();

			//cout << "Predicting (" << wstart << "," << wsize << ") --> " << predictedDeltas[indice].COUPLEGetCouplePrediction() << ", min=" << dLimit << endl;

			list<pair<int, int> > L;
			for (int i = 0; i < N.size(); i++) {
				if (i<wstart || i>wstart + wsize - 1) {
					x[S[i]][i].setLB(1);
					x[S[i]][i].setUB(1);
					L.push_back(pair<int, int>(S[i], i));
				}
			}
			clock_t sp_start = clock();
			cpx.solve();
			sp_time = ((double)(clock() - sp_start) / CLOCKS_PER_SEC);
			avg_sp_time += sp_time;
			sp_cnt++;
			if (sp_time > max_sp_time) {
				max_sp_time = sp_time;
			}

			//If CPlex sequence has a better objective function
			if ((double)cpx.getObjValue() < (double)(bestobj - 0.0001)) {
				dPerImp = 100.0*(double)((bestobjiter - (int)cpx.getObjValue())) / (double)(bestobjiter);
				dPerImpInit = 100.0*(double)((uiObjInit - (int)cpx.getObjValue())) / (double)(uiObjInit);
				bestobj = (int)(cpx.getObjValue());
				get_sequence(cpx, x, N, S);
				cout << "(prediction=" << predictedDeltas[indice].COUPLEGetCouplePrediction() << ") Improved to " << bestobj <<
					" at time " << ((double)(clock() - start_time)) / CLOCKS_PER_SEC << endl;
				for (uiLoop = 0; uiLoop < N.size(); uiLoop++)
					bestS[uiLoop] = S[uiLoop];
				uiBestr = wstart;
				uiBesth = wsize;
				bImproved = true;
			}
			while (!L.empty()) {
				x[L.front().first][L.front().second].setLB(0);
				x[L.front().first][L.front().second].setUB(1);
				L.pop_front();
			}
			/*FILE *fichierStats = fopen("stats.txt", "a");
			fprintf(fichierStats, "%lf %d \n", prediction, realimproved);
			fclose(fichierStats);*/

			indice++;
		}
		uiIter++;
	}
}


/**
* New matheuristic hybrided with the predictor. Sort all deltas in descending order. Randomized R values and 4<=H<=12. Optimise only the best prediction .
*
* \param cpx the CPlex instance
* \param x the list of constraints
* \param N the problem instance
* \param S the sequence to optimize
* \param bestobj the objective value returned by the RBS heuristic
* \param model the predictor
* \param timelim the time limit
*/
bool searchBestPrediction(IloCplex cpx, IloIntVarArray x[], vector<Job>& N, vector<int>& S, int&bestobj, CModelPrediction& model, double timelim = 1e20)
{
	const int max_Nr_win = N.size() - 4;
	unsigned int wsize;
	unsigned int uiIter = 0;
	unsigned int uiLoop;
	FILE *fichier;
	vector<int> bestS(N.size());
	unsigned int uiBestr = 0, uiBesth = 0;
	unsigned int uiObjInit = bestobj, bestobjiter;
	double dPerImpInit, dPerImp;
	bool bImproved;
	CStatistics statistics;
	vector<CCouple> predictedDeltas;


	vector<bool> used(N.size());

	bool done = false;
	cout << "initial=" << bestobj << endl;

	clock_t start_time = clock();

	while (!done) {
		done = true;
		for (int i = 0; i < max_Nr_win && done; ++i)
			done = done && used[i];
		if (!done) {
			int wstart;
			list<pair<int, int> > L;
			do {
				wstart = rand() % max_Nr_win;
			} while (used[wstart]);

			predictedDeltas.clear();
			
			//Test every window size from h=4 to h=12
			int seqSize = N.size();
			for (wsize = 4; wsize <= seqSize-wstart && wsize <= 12; wsize++)
			{
				//Compute statistics
				statistics.STATComputeStatistics(&S, S.size(), wstart, wsize);
				map<string, double> features = statistics.STATGetStatistics();

				double prediction = model.MODELPredict(features);

				CCouple couple(wstart, wsize, prediction);
				predictedDeltas.push_back(couple);
				
			}

			sort(predictedDeltas.begin(), predictedDeltas.end());
			wstart = predictedDeltas[0].COUPLEGetR();
			wsize = predictedDeltas[0].COUPLEGetH();

			for (int i = 0; i < N.size(); i++) {
				if (i<wstart || i>wstart + wsize - 1) {
					x[S[i]][i].setLB(1);
					x[S[i]][i].setUB(1);
					L.push_back(pair<int, int>(S[i], i));
				}
			}
			clock_t sp_start = clock();
			cpx.solve();
			sp_time = ((double)(clock() - sp_start) / CLOCKS_PER_SEC);
			avg_sp_time += sp_time;
			sp_cnt++;
			if (sp_time > max_sp_time) {
				max_sp_time = sp_time;
			}
			if (cpx.getObjValue() < bestobj) {
				bestobj = (int)cpx.getObjValue();
				get_sequence(cpx, x, N, S);
				cout << "Improved to " << bestobj <<
					" at time " << ((double)(clock() - start_time)) / CLOCKS_PER_SEC << endl;
				fill(used.begin(), used.end(), false);
			}
			while (!L.empty()) {
				x[L.front().first][L.front().second].setLB(0);
				x[L.front().first][L.front().second].setUB(1);
				L.pop_front();
			}
			FILE* fichier = fopen("Graphe.txt", "at+");
			fprintf(fichier, "%d %.3f %d %d %d \n", bestobj, ((double)(clock() - start_time)) / CLOCKS_PER_SEC, uiIter,wstart,wsize);
			fclose(fichier);
		}
		
		uiIter++;
		clock_t stop_time = clock();
		if (((stop_time - start_time) / CLOCKS_PER_SEC) > timelim)
			break;
	}
}

/**
 * New matheuristic hybrided with the predictor. Sort all deltas in descending order. Iterates through all R values and 4<=H<=12. Stops when 70% of the best deltas have been tested
 *
 * \param cpx the CPlex instance
 * \param x the list of constraints
 * \param N the problem instance
 * \param S the sequence to optimize
 * \param bestobj the objective value returned by the RBS heuristic
 * \param model the predictor
 * \param timelim the time limit
 */
bool searchMetaHybrid(IloCplex cpx, IloIntVarArray x[], vector<Job>& N, vector<int>& S, int&bestobj, CModelPrediction& model)
{
	unsigned int wsize;
	unsigned int wstart;
	unsigned int uiIter = 0;
	unsigned int uiLoop;
	FILE *fichier;
	vector<int> bestS(N.size());
	unsigned int uiBestr = 0, uiBesth = 0;
	unsigned int uiObjInit = bestobj, bestobjiter;
	double dPerImpInit, dPerImp;
	bool bImproved;
	CStatistics statistics;
	vector<CCouple> predictedDeltas;

	cout << "initial=" << bestobj << endl;
	for (uiLoop = 0; uiLoop < N.size(); uiLoop++)
		bestS[uiLoop] = S[uiLoop];

	clock_t start_time = clock();

	bImproved = true;
	printf("Starting search\n");


	//While solution is improved (we're not in a local minimum)
	while (bImproved) {
		printf("Iteration %d\n", uiIter + 1);
		bImproved = false;
		bestobjiter = bestobj;
		predictedDeltas.clear();

		//Test every window size from h=4 to h=12
		for (wsize = 4; wsize <= 12; wsize++)
		{
			//Test every possible window start position
			for (wstart = 0; wstart < N.size() - wsize; wstart++)
			{
				//Compute statistics
				statistics.STATComputeStatistics(&S, S.size(), wstart, wsize);
				map<string, double> features = statistics.STATGetStatistics();

				double prediction = model.MODELPredict(features);

				CCouple couple(wstart, wsize, prediction);
				predictedDeltas.push_back(couple);
			}
		}

		sort(predictedDeltas.begin(), predictedDeltas.end());
		reverse(predictedDeltas.begin(), predictedDeltas.end());

		unsigned int indice = 0;
		while (bImproved == false && (predictedDeltas[indice].COUPLEGetCouplePrediction() > 0 || indice < (double)predictedDeltas.size()*(double)0.7)) {

			wstart = predictedDeltas[indice].COUPLEGetR();
			wsize = predictedDeltas[indice].COUPLEGetH();

			//cout << "Predicting (" << wstart << "," << wsize << ") --> " << predictedDeltas[indice].COUPLEGetCouplePrediction() << ",i=" << indice << ", imax=" << (double)predictedDeltas.size()*(double)0.7 << endl;

			list<pair<int, int> > L;
			for (int i = 0; i < N.size(); i++) {
				if (i<wstart || i>wstart + wsize - 1) {
					x[S[i]][i].setLB(1);
					x[S[i]][i].setUB(1);
					L.push_back(pair<int, int>(S[i], i));
				}
			}
			clock_t sp_start = clock();
			cpx.solve();
			sp_time = ((double)(clock() - sp_start) / CLOCKS_PER_SEC);
			avg_sp_time += sp_time;
			sp_cnt++;
			if (sp_time > max_sp_time) {
				max_sp_time = sp_time;
			}

			//If CPlex sequence has a better objective function
			if ((double)cpx.getObjValue() < (double)(bestobj - 0.0001)) {
				dPerImp = 100.0*(double)((bestobjiter - (int)cpx.getObjValue())) / (double)(bestobjiter);
				dPerImpInit = 100.0*(double)((uiObjInit - (int)cpx.getObjValue())) / (double)(uiObjInit);
				bestobj = (int)(cpx.getObjValue());
				get_sequence(cpx, x, N, S);
				cout << "(prediction=" << predictedDeltas[indice].COUPLEGetCouplePrediction() << ") Improved to " << bestobj <<
					" at time " << ((double)(clock() - start_time)) / CLOCKS_PER_SEC << endl;
				for (uiLoop = 0; uiLoop < N.size(); uiLoop++)
					bestS[uiLoop] = S[uiLoop];
				uiBestr = wstart;
				uiBesth = wsize;
				bImproved = true;
			}
			while (!L.empty()) {
				x[L.front().first][L.front().second].setLB(0);
				x[L.front().first][L.front().second].setUB(1);
				L.pop_front();
			}
			/*FILE *fichierStats = fopen("stats.txt", "a");
			fprintf(fichierStats, "%lf %d \n", prediction, realimproved);
			fclose(fichierStats);*/

			indice++;
		}
		uiIter++;
	}
}




/**
* The original matheuristic  without the predictor. Randomized R values and H fixed to 12.
*
* \param cpx the CPlex instance
* \param x the list of constraints
* \param N the problem instance
* \param S the sequence to optimize
* \param bestobj the objective value returned by the RBS heuristic
* \param timelim the time limit
*/
bool searchOrig(IloCplex cpx, IloIntVarArray x[], vector<Job>& N, vector<int>& S, int&bestobj, double timelim = 1e20)
{
	const int wsize = 12;
	const int max_Nr_win = N.size() - wsize;
	unsigned int uiIter = 0;
	CStatistics statistics;

	vector<bool> used(N.size());

	bool done = false;
	cout << "initial=" << bestobj << endl;

	clock_t start_time = clock();

	while (!done) {
		done = true;
		for (int i = 0; i < max_Nr_win && done; ++i)
			done = done && used[i];
		if (!done) {
			int wstart;
			list<pair<int, int> > L;
			do {
				wstart = rand() % max_Nr_win;
			} while (used[wstart]);



			for (int i = 0; i < N.size(); i++) {
				if (i<wstart || i>wstart + wsize - 1) {
					x[S[i]][i].setLB(1);
					x[S[i]][i].setUB(1);
					L.push_back(pair<int, int>(S[i], i));
				}
			}
			clock_t sp_start = clock();
			cpx.solve();
			sp_time = ((double)(clock() - sp_start) / CLOCKS_PER_SEC);
			avg_sp_time += sp_time;
			sp_cnt++;
			if (sp_time > max_sp_time) {
				max_sp_time = sp_time;
			}
			if (cpx.getObjValue() < bestobj) {
				bestobj = (int)cpx.getObjValue();
				get_sequence(cpx, x, N, S);
				cout << "Improved to " << bestobj <<
					" at time " << ((double)(clock() - start_time)) / CLOCKS_PER_SEC << endl;
				fill(used.begin(), used.end(), false);
			}
			while (!L.empty()) {
				x[L.front().first][L.front().second].setLB(0);
				x[L.front().first][L.front().second].setUB(1);
				L.pop_front();
			}
		}
		FILE* fichier = fopen("Graphe.txt", "at+");
		fprintf(fichier, "%d %.3f %d \n", bestobj, ((double)(clock() - start_time)) / CLOCKS_PER_SEC, uiIter);
		fclose(fichier);
		uiIter++;
		clock_t stop_time = clock();
		if (((stop_time - start_time) / CLOCKS_PER_SEC) > timelim)
			break;
	}
}


/**
* The random matheuristic . Randomized R values and H fixed to 12. we decide at random to optimize. 
*
* \param cpx the CPlex instance
* \param x the list of constraints
* \param N the problem instance
* \param S the sequence to optimize
* \param bestobj the objective value returned by the RBS heuristic
* \param timelim the time limit
*/
bool searchRandomMath(IloCplex cpx, IloIntVarArray x[], vector<Job>& N, vector<int>& S, int&bestobj, double timelim = 1e20)
{
	const int wsize = 12;
	const int max_Nr_win = N.size() - wsize;
	unsigned int uiIter = 0;
	CStatistics statistics;

	vector<bool> used(N.size());

	bool done = false;
	cout << "initial=" << bestobj << endl;

	clock_t start_time = clock();

	while (!done) {
		done = true;
		for (int i = 0; i < max_Nr_win && done; ++i)
			done = done && used[i];
		if (!done) {
			int wstart;
			list<pair<int, int> > L;
			do {
				wstart = rand() % max_Nr_win;
			} while (used[wstart]);

			

			double prob = rand() / double (RAND_MAX);
			//If the probability is greater than 0.5, we run CPlex
			if (prob > 0.5f) {

				for (int i = 0; i < N.size(); i++) {
					if (i<wstart || i>wstart + wsize - 1) {
						x[S[i]][i].setLB(1);
						x[S[i]][i].setUB(1);
						L.push_back(pair<int, int>(S[i], i));
					}
				}
				clock_t sp_start = clock();
				cpx.solve();
				sp_time = ((double)(clock() - sp_start) / CLOCKS_PER_SEC);
				avg_sp_time += sp_time;
				sp_cnt++;
				if (sp_time > max_sp_time) {
					max_sp_time = sp_time;
				}
				if (cpx.getObjValue() < bestobj) {
					bestobj = (int)cpx.getObjValue();
					get_sequence(cpx, x, N, S);
					cout << "Improved to " << bestobj <<
						" at time " << ((double)(clock() - start_time)) / CLOCKS_PER_SEC << endl;
					fill(used.begin(), used.end(), false);
				}
				while (!L.empty()) {
					x[L.front().first][L.front().second].setLB(0);
					x[L.front().first][L.front().second].setUB(1);
					L.pop_front();
				}
			}
			else {
				//cout << "Pas d'amelioration prevue" << endl;
			}
			FILE* fichier = fopen("Graphe.txt", "at+");
			fprintf(fichier, "%d %.3f %d %.3f \n", bestobj, ((double)(clock() - start_time)) / CLOCKS_PER_SEC, uiIter, prob);
			fclose(fichier);
			uiIter++;
		}
		clock_t stop_time = clock();
		if (((stop_time - start_time) / CLOCKS_PER_SEC) > timelim)
			break;
	}
}


/**
 * The original matheuristic hybrided with the predictor. Randomized R values and H fixed to 12.
 *
 * \param cpx the CPlex instance
 * \param x the list of constraints
 * \param N the problem instance
 * \param S the sequence to optimize
 * \param bestobj the objective value returned by the RBS heuristic
 * \param model the predictor
 * \param timelim the time limit
 */
bool searchMetaOrig(IloCplex cpx, IloIntVarArray x[], vector<Job>& N, vector<int>& S, int&bestobj, CModelPrediction& model, double timelim = 1e20)
{
	const int wsize = 12;
	const int max_Nr_win = N.size() - wsize;
	unsigned int uiIter = 0;
	unsigned int uiPrediction = 0;
	unsigned int uiPreRight = 0;
	unsigned int uiPrePositif = 0;
	CStatistics statistics;

	vector<bool> used(N.size());

	bool done = false;
	cout << "initial=" << bestobj << endl;

	clock_t start_time = clock();

	while (!done) {
		done = true;
		for (int i = 0; i < max_Nr_win && done; ++i)
			done = done && used[i];
		if (!done) {
			int wstart;
			list<pair<int, int> > L;
			do {
				wstart = rand() % max_Nr_win;
			} while (used[wstart]);

			statistics.STATComputeStatistics(&S, S.size(), wstart, wsize);
			map<string, double> features = statistics.STATGetStatistics();

			double prediction = model.MODELPredict(features);
			uiPrediction++;

			//If model predicts an improvement, we run CPlex
			//cout << prediction << endl;
			if (prediction > 0.5f) {
				uiPrePositif++;
				for (int i = 0; i < N.size(); i++) {
					if (i<wstart || i>wstart + wsize - 1) {
						x[S[i]][i].setLB(1);
						x[S[i]][i].setUB(1);
						L.push_back(pair<int, int>(S[i], i));
					}
				}
				clock_t sp_start = clock();
				cpx.solve();
				sp_time = ((double)(clock() - sp_start) / CLOCKS_PER_SEC);
				avg_sp_time += sp_time;
				sp_cnt++;
				if (sp_time > max_sp_time) {
					max_sp_time = sp_time;
				}
				if (cpx.getObjValue() < bestobj) {
					bestobj = (int)cpx.getObjValue();
					get_sequence(cpx, x, N, S);
					uiPreRight++;
					cout << "Improved to " << bestobj <<
						" at time " << ((double)(clock() - start_time)) / CLOCKS_PER_SEC << endl;
					fill(used.begin(), used.end(), false);
				}
				while (!L.empty()) {
					x[L.front().first][L.front().second].setLB(0);
					x[L.front().first][L.front().second].setUB(1);
					L.pop_front();
				}
			}
			else {
				//cout << "Pas d'amelioration prevue" << endl;
			}
			uiIter++;
		}
		clock_t stop_time = clock();
		if (((stop_time - start_time) / CLOCKS_PER_SEC) > timelim)
			break;
	}
	FILE *fichier;
	fichier = fopen("Prediction.txt", "at+");
	fprintf(fichier, "Prediction: ALL = %d, positive prediction =  %d,  positive correct prediction %d\n ",uiPrediction, uiPrePositif, uiPreRight);
	fclose(fichier);
}

/**
 * The bruteforce matheuristic hybrided with the predictor. Iterates through all R values and 4<=H<=16.
 *
 * \param cpx the CPlex instance
 * \param x the list of constraints
 * \param N the problem instance
 * \param S the sequence to optimize
 * \param bestobj the objective value returned by the RBS heuristic
 * \param model the predictor
 * \param timelim the time limit
 */
bool search(IloCplex cpx, IloIntVarArray x[], vector<Job>& N, vector<int>& S, int&bestobj, CModelPrediction& model, double timelim = 1e20)
{
	unsigned int wsize;
	unsigned int wstart;
	unsigned int uiIter = 0;
	unsigned int uiLoop;
	FILE *fichier;
	vector<int> bestS(N.size()), Sp(N.size());
	unsigned int uiBestr = 0, uiBesth = 0;
	unsigned int uiObjInit = bestobj, bestobjiter;
	double dPerImpInit, dPerImp;
	bool bImproved;
	CStatistics statistics;

	cout << "initial=" << bestobj << endl;
	for (uiLoop = 0; uiLoop < N.size(); uiLoop++)
		bestS[uiLoop] = S[uiLoop];

	clock_t start_time = clock();

	bImproved = true;
	printf("Starting search\n");


	//While number of iterations is inferior to MAX_ITER and solution is improved (we're not in a local minimum)
	while (uiIter <= MAX_ITER && bImproved) {
		printf("Iteration %d\n", uiIter + 1);
		bImproved = false;
		bestobjiter = bestobj;

		//Test every window size from h=4 to h=16
		for (wsize = 4; wsize <= 16; wsize++)
		{
			printf("Size : %d\n", wsize);

			//Test every possible window start position
			for (wstart = 0; wstart < N.size() - wsize; wstart++)
			{
				statistics.STATComputeStatistics(&S, S.size(), wstart, wsize);
				map<string, double> features = statistics.STATGetStatistics();

				//If model predicts an improvement, we run CPlex
				double prediction = model.MODELPredict(features);

				bool realimproved = false;

				if (prediction > 0) {

					list<pair<int, int> > L;
					for (int i = 0; i < N.size(); i++) {
						if (i<wstart || i>wstart + wsize - 1) {
							x[S[i]][i].setLB(1);
							x[S[i]][i].setUB(1);
							L.push_back(pair<int, int>(S[i], i));
						}
					}
					clock_t sp_start = clock();
					cpx.solve();
					sp_time = ((double)(clock() - sp_start) / CLOCKS_PER_SEC);
					avg_sp_time += sp_time;
					sp_cnt++;
					if (sp_time > max_sp_time) {
						max_sp_time = sp_time;
					}

					//If CPlex sequence has a better objective function
					if ((double)cpx.getObjValue() < (double)(bestobj - 0.0001)) {
						dPerImp = 100.0*(double)((bestobjiter - (int)cpx.getObjValue())) / (double)(bestobjiter);
						dPerImpInit = 100.0*(double)((uiObjInit - (int)cpx.getObjValue())) / (double)(uiObjInit);
						bestobj = (int)(cpx.getObjValue());
						get_sequence(cpx, x, N, Sp);
						cout << "(prediction=" << prediction << ") Improved to " << bestobj <<
							" at time " << ((double)(clock() - start_time)) / CLOCKS_PER_SEC << endl;
						for (uiLoop = 0; uiLoop < N.size(); uiLoop++)
							bestS[uiLoop] = Sp[uiLoop];
						uiBestr = wstart;
						uiBesth = wsize;
						bImproved = true;
						realimproved = true;
					}
					while (!L.empty()) {
						x[L.front().first][L.front().second].setLB(0);
						x[L.front().first][L.front().second].setUB(1);
						L.pop_front();
					}
					FILE *fichierStats = fopen("stats.txt", "a");
					fprintf(fichierStats, "%lf %d \n", prediction, realimproved);
					fclose(fichierStats);
				}
				else {
					//cout << "Pas d'amelioration prevue" << endl;
				}

			}
		}
		if (bImproved)
		{
			fichier = fopen("Database.txt", "at+");
			fprintf(fichier, "%d %d %d %3.5lf %3.5lf ", N.size(), uiBestr, uiBesth, dPerImp, dPerImpInit);
			for (uiLoop = 0; uiLoop < N.size(); uiLoop++)
				fprintf(fichier, "%d %d ", N[bestS[uiLoop]].ptime[0], N[bestS[uiLoop]].ptime[1]);
			for (uiLoop = 0; uiLoop < N.size(); uiLoop++)
				fprintf(fichier, "%d %d ", N[S[uiLoop]].ptime[0], N[S[uiLoop]].ptime[1]);
			fprintf(fichier, "\n");
			fclose(fichier);
			for (uiLoop = 0; uiLoop < N.size(); uiLoop++)
				S[uiLoop] = bestS[uiLoop];
		}
		uiIter++;

		clock_t stop_time = clock();
		if (((stop_time - start_time) / CLOCKS_PER_SEC) > timelim)
			break;
	}
}

/**
 * Reads the sequence returned by the RBS heuristic
 *
 * \param fname the path to the file containg the sequence
 * \return S the sequence
 */
int read_sequence(const char *fname, vector<int>& S)
{
	ifstream ifs(fname);
	int obj;

	if (ifs.is_open()) {
		ifs >> obj;
		for (int i = 0; i < S.size(); ++i)
			ifs >> S[i];
	}
	else {
		char buf[128];
		snprintf(buf, 127, "read_sequence(): cannot open %s", fname);
		throw(buf);
	}
	return obj;
}

/**
* Generate a random sequence
*
* \param fname the path to the file containg the sequence
* \return S the sequence
*/
int read_random_sequence(vector<int>& S) {
	//vector<int> numbers = {16,11,19,2,12,9,10,18,6,1,5,15,8,7,4,3,13,17,14,0};
	vector<int> numbers;
	for (unsigned int uiBoucle = 0; uiBoucle < S.size(); uiBoucle++) {
		numbers.push_back(uiBoucle);
	}
	random_shuffle(numbers.begin(), numbers.end());

	for (unsigned int uiBoucle = 0; uiBoucle < S.size(); uiBoucle++) {
		S[uiBoucle] = numbers[uiBoucle];
		cout << S[uiBoucle] << "-";
	}
	cout << endl;

	unsigned int uiSTATCm1 = 0;
	unsigned int uiSTATCm2 = 0;
	unsigned int sommeCm2 = 0;
	for (unsigned int uiBoucle = 0; uiBoucle < S.size(); uiBoucle++)
	{
		uiSTATCm1 += N[S[uiBoucle]].ptime[0];
		if (uiSTATCm2 > uiSTATCm1)
			uiSTATCm2 += N[S[uiBoucle]].ptime[1];
		else uiSTATCm2 = uiSTATCm1 + N[S[uiBoucle]].ptime[1];
		sommeCm2 += uiSTATCm2;
	}
	cout << sommeCm2 << endl;
	return sommeCm2;
}

/**
 * Entry point of the program.
 */
int main(int argc, char *argv[])
{
	int res = EXIT_SUCCESS;
	try {
		if (argc < 6) {
			throw("Please provide instance file name, model file name, model type, matheuristic_number, time limit , RBS and random seed (optional)");
		}

		if (argc == 8) {
			srand(atoi(argv[7]));
		}
		else {
			srand(time(NULL));
		}
		rand();
		double tlim = atof(argv[5]);

		int iMatheuristicNumber = atoi(argv[4]);
		int RBS = atoi(argv[6]);

		string sPath = argv[1];
		read_jobs(sPath.c_str(), N);
		string sModelType = argv[3];
		

		cout << N.size() << " jobs" << endl;

		IloEnv env;
		IloModel model(env);

		IloIntVarArray x[MAX_JOBS];
		for (int i = 0; i < N.size(); ++i) {
			char buf[128];
			x[i] = IloIntVarArray(env, N.size(), 0.0, 1.0);
			for (int j = 0; j < N.size(); ++j) {
				snprintf(buf, 127, "x(%d,%d)", i, j);
				x[i][j].setName(buf);
			}
		}

		IloNumVarArray Ctime[2];
		Ctime[0] = IloNumVarArray(env, N.size(), 0.0, IloInfinity);
		Ctime[1] = IloNumVarArray(env, N.size(), 0.0, IloInfinity);
		for (int i = 0; i < N.size(); ++i) {
			char buf[128];
			snprintf(buf, 127, "C(0,%d)", i);
			Ctime[0][i].setName(buf);
			snprintf(buf, 127, "C(1,%d)", i);
			Ctime[1][i].setName(buf);
		}

		model.add(IloMinimize(env, IloSum(Ctime[1])));

		for (int i = 0; i < N.size(); ++i) {
			IloExpr e(env);
			for (int j = 0; j < N.size(); ++j) {
				e += x[i][j];
			}
			model.add(e == 1.0);
			e.end();
		}

		for (int j = 0; j < N.size(); ++j) {
			IloExpr e(env);
			for (int i = 0; i < N.size(); ++i) {
				e += x[i][j];
			}
			model.add(e == 1.0);
		}


		for (int pos = 0; pos < N.size(); ++pos) {
			IloExpr tmp(env), tmp2(env), tmp3(env);
			tmp.clear();
			tmp2.clear();
			tmp3.clear();
			for (int i = 0; i < N.size(); ++i) {
				tmp += N[i].ptime[0] * x[i][pos];
				tmp2 += N[i].ptime[1] * x[i][pos];
				tmp3 += N[i].ptime[1] * x[i][pos];
			}
			if (!pos) {
				model.add(Ctime[0][0] >= tmp);
				model.add(Ctime[1][0] >= Ctime[0][0] + tmp2);
			}
			else {
				model.add(Ctime[0][pos] >= Ctime[0][pos - 1] + tmp);
				model.add(Ctime[1][pos] >= Ctime[1][pos - 1] + tmp2);
				model.add(Ctime[1][pos] >= Ctime[0][pos] + tmp3);
			}
		}
		IloCplex cpx(model);

		cpx.setParam(IloCplex::Param::ParamDisplay, 0);
		cpx.setParam(IloCplex::MIPDisplay, 0);
		cpx.setParam(IloCplex::EpGap, 0);

		vector<int> S(N.size());

		wchar_t Hname[] = L"RBS_FLOWSHOP.exe";
		_spawnl(P_WAIT, "RBS_FLOWSHOP.exe", "RBS_FLOWSHOP.exe", sPath.c_str(), NULL);
		char ubname[128];
		snprintf(ubname, 127, "ub.rep");
		int cur_best_obj =0;

		if (RBS == 1) {
			cur_best_obj = read_sequence(ubname, S);
		}
		else {
			cur_best_obj = read_random_sequence(S);
		}

		initial_obj = cur_best_obj;
		clock_t t_start = clock();
		CModelPrediction modelPrediction(argv[2], sModelType != "lasso");

		switch (iMatheuristicNumber) {
		case 1:
			cout << "Using bruteforce matheuristic with predictor..." << endl << endl;
			search(cpx, x, N, S, cur_best_obj, modelPrediction, tlim);
			break;
		case 2:
			cout << "Using original matheuristic with predictor..." << endl << endl;
			searchMetaOrig(cpx, x, N, S, cur_best_obj, modelPrediction, tlim);
			break;
		case 3:
			cout << "Using new matheuristic (ordered deltas, stops at 70% of the delta list) with predictor..." << endl << endl;
			searchMetaHybrid(cpx, x, N, S, cur_best_obj, modelPrediction);
			break;
		case 4:
			cout << "Using new matheuristic (ordered deltas, stops at 10% of the worst delta) with predictor..." << endl << endl;
			searchMetaHybrid10Percent(cpx, x, N, S, cur_best_obj, modelPrediction);
			break;
		case 5:
			cout << "Using original matheuristic without predictor..." << endl << endl;
			searchOrig(cpx, x, N, S, cur_best_obj, tlim);
			break;
		case 6:
			cout << "Using random matheuristic without predictor..." << endl << endl;
			searchRandomMath(cpx, x, N, S, cur_best_obj, tlim);
			break;
		case 7:
			cout << "Using best prediction matheuristic (random r , best prediction among 4<= h <=12) with predictor  ..." << endl << endl;
			searchBestPrediction(cpx, x, N, S, cur_best_obj, modelPrediction, tlim);
			break;
		default:
			cout << "ERROR : Invalid matheuristic, value can be 1, 2, 3, 4, 5, 6 or 7." << endl << endl;
		}
		
		

		cout << "INITIALOBJ=" << initial_obj << endl;
		cout << "BESTOBJ=" << cur_best_obj << endl;
		cout << "Total time=" << ((double)(clock() - t_start) / CLOCKS_PER_SEC) << endl;
		cout << "Avg subproblem time=" << avg_sp_time / sp_cnt << endl;
		cout << "Max subproblem time=" << max_sp_time << endl;

		char sfname[128];
		snprintf(sfname, 127, "%s.seq", argv[1]);
		ofstream ofs(sfname);
		if (ofs.is_open()) {
			ofs << cur_best_obj << "  ";
			for (int i = 0; i < N.size(); i++) {
				ofs << S[i] << " ";
			}
			ofs << endl;
		}
	}

	catch (const char *s) {
		cerr << s << endl;
		res = EXIT_FAILURE;
	}
	catch (IloException e) {
		cerr << e << endl;
		res = EXIT_FAILURE;
	}

	return res;
}
