/**
 * \brief Class for storing a window (R,H) associated to a predicted delta.
 */

class CCouple
{
private:
	unsigned int uiCOUPLER; // the window position
	unsigned int uiCOUPLEH; // the window size
	double dCOUPLEPrediction; // the prediction
public:
	/**
	 * Constructs a CCouple for R (window position), H (window size) and prediction (predicted delta).
	 *
	 * \param r the window position.
	 * \param h the window size.
	 * \param prediction the predicted delta.
	 */
	CCouple(unsigned int r, unsigned int h, double prediction);

	/**
	 * Destructor.
	 */
	~CCouple();

	/**
	 * Returns the window position.
	 *
	 * \return R (the window position).
	 */
	unsigned int COUPLEGetR();

	/**
	 * Returns the window size.
	 *
	 * \return H (the window size).
	 */
	unsigned int COUPLEGetH();

	/**
	 * Returns the prediction associated to the couple.
	 *
	 * \return delta associated to the prediction.
	 */
	double COUPLEGetCouplePrediction();

	/**
	 * Operator < overload, used for sorting.
	 */
	bool operator <(const CCouple &couple) const;
};

