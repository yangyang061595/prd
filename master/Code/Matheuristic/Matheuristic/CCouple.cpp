#include "CCouple.h"

/**
 * Constructs a CCouple for R (window position), H (window size) and prediction (predicted delta).
 *
 * \param r the window position.
 * \param h the window size.
 * \param prediction the predicted delta.
 */
CCouple::CCouple(unsigned int uiR, unsigned int uiH, double prediction)
{
	uiCOUPLER = uiR;
	uiCOUPLEH = uiH;
	dCOUPLEPrediction = prediction;
}

/**
 * Returns the window position.
 *
 * \return R (the window position).
 */
unsigned int CCouple::COUPLEGetR() {
	return uiCOUPLER;
}

/**
 * Returns the window size.
 *
 * \return H (the window size).
 */
unsigned int CCouple::COUPLEGetH() {
	return uiCOUPLEH;
}

/**
 * Returns the prediction associated to the couple.
 *
 * \return delta associated to the prediction.
 */
double CCouple::COUPLEGetCouplePrediction()
{
	return dCOUPLEPrediction;
}

/**
 * Operator < overload, used for sorting.
 */
bool CCouple::operator<(const CCouple & couple) const
{
	return dCOUPLEPrediction < couple.dCOUPLEPrediction;
}

/**
 * Destructor.
 */
CCouple::~CCouple()
{
}
