#include "Cstatistics.h"
#include <chrono>

/**
 * Compute the features on a given input sequence.
 *
 * \param pSeq pointeur to the sequence to evaluate.
 * \param uiSize size of the sequence.
 * \param uiR starting position of the window of positions.
 * \param uiH length of the window of positions.
 */
void CStatistics::STATComputeStatistics(vector<int> * pSeq, unsigned int uiSize, unsigned int uiR, unsigned int uiH)
{
	unsigned __int64 now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	int iMinPt1, iMaxPt1, iMinPt2, iMaxPt2;
	iMaxPt1 = iMaxPt2 = 0;
	iMinPt1 = iMinPt2 = INT_MAX;

	pSTATSeq = pSeq;

	for (unsigned int uiLoop = 0; uiLoop < uiSize; uiLoop++) {
		if (N[uiLoop].ptime[0] > iMaxPt1) {
			iMaxPt1 = N[uiLoop].ptime[0];
		}
		if (N[uiLoop].ptime[1] > iMaxPt2) {
			iMaxPt2 = N[uiLoop].ptime[1];
		}
		if (N[uiLoop].ptime[0] < iMinPt1) {
			iMinPt1 = N[uiLoop].ptime[0];
		}
		if (N[uiLoop].ptime[1] < iMinPt2) {
			iMinPt2 = N[uiLoop].ptime[1];
		}
	}

	uiSTATSize = uiSize;
	uiSTATR = uiR;
	uiSTATH = uiH;
	STATComputeFam1();
	STATComputeFam2and7();
	STATComputeFam3and8();
	STATComputeFam4and9();
	STATComputeFam6and10();
	STATComputeFam12();
	STATComputeProcessingTime(iMinPt1, iMaxPt1, iMinPt2, iMaxPt2);


	//system("pause");

	unsigned __int64 nowf = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	//cout << nowf-now << endl;
}

/**
 * Returns the computed features.
 *
 * \return features {key,value}.
 */
map<string, double> CStatistics::STATGetStatistics()
{
	map<string, double> features;
	// Writing Family 1 statistics
	features["C1"] = (double)uiSTATCm1/(double)uiSTATSize;
	features["C2"] = (double)uiSTATCm2/(double)uiSTATSize;
	features["C3"] = (double)uiSTATP1/(double)uiSTATSize;
	features["C4"] = (double)uiSTATP2/(double)uiSTATSize;
	// Writing Family 2 statistics
	features["C5"] = dSTATQ31;
	features["C6"] = dSTATQ32;
	// Writing Family 3 statistics
	features["C7"] = dSTATitQ31;
	features["C8"] = dSTATitQ32;
	// Writing Family 4 statistics
	features["C9"] = dSTATprQ31;
	features["C10"] = dSTATprQ32;
	// Writing Family 5 statistics
	//features["C11"] = dSTATrelax;
	// Writing Family 6 statistics
	features["C121"] = dSTATitRQ31;
	features["C122"] = dSTATitRQ32;
	// Writing Family 7 statistics
	features["C13"] = dSTATQ101;
	features["C14"] = dSTATQ102; 
	features["C15"] = dSTATQ103;
	features["C16"] = dSTATQ104;
	features["C17"] = dSTATQ105;
	features["C18"] = dSTATQ106;
	features["C19"] = dSTATQ107;
	features["C20"] = dSTATQ108;
	features["C21"] = dSTATQ109;
	// Writing Family 8 statistics
	features["C22"] = dSTATitQ101;
	features["C23"] = dSTATitQ102;
	features["C24"] = dSTATitQ103;
	features["C25"] = dSTATitQ104;
	features["C26"] = dSTATitQ105;
	features["C27"] = dSTATitQ106;
	features["C28"] = dSTATitQ107;
	features["C29"] = dSTATitQ108;
	features["C30"] = dSTATitQ109;
	// Writing Family 9 statistics
	features["C31"] = dSTATprQ101;
	features["C32"] = dSTATprQ102;
	features["C33"] = dSTATprQ103;
	features["C34"] = dSTATprQ104;
	features["C35"] = dSTATprQ105;
	features["C36"] = dSTATprQ106;
	features["C37"] = dSTATprQ107;
	features["C38"] = dSTATprQ108;
	features["C39"] = dSTATprQ109;
	// Writing Family 10 statistics
	features["C40"] = dSTATitRQ101;
	features["C41"] = dSTATitRQ102;
	features["C42"] = dSTATitRQ103;
	features["C43"] = dSTATitRQ104;
	features["C44"] = dSTATitRQ105;
	features["C45"] = dSTATitRQ106;
	features["C46"] = dSTATitRQ107;
	features["C47"] = dSTATitRQ108;
	features["C48"] = dSTATitRQ109;
	// Writing Family 11 statistics
	features["C49"] = dSTATmin;
	features["C50"] = dSTATmax;
	features["C51"] = dSTATsd;
	features["C52"] = dSTATitmin;
	features["C53"] = dSTATitmax;
	features["C54"] = dSTATitsd;
	features["C55"] = dSTATprmin;
	features["C56"] = dSTATprmax;
	features["C57"] = dSTATprsd;
	features["C58"] = dSTATitRmin;
	features["C59"] = dSTATitRmax;
	features["C60"] = dSTATitRsd;
	features["C61"] = dSTATwindowPI1min;
	features["C62"] = dSTATwindowPI1max;
	features["C63"] = dSTATwindowPI1sd;
	features["C64"] = dSTATwindowPI2min;
	features["C65"] = dSTATwindowPI2max;
	features["C66"] = dSTATwindowPI2sd;
	// Writing Family 12 statistics
	features["C67"] = dSTATSRPTMeanWindow;
	features["C68"] = dSTATSRPTMaxWindow;
	features["C69"] = dSTATSRPTMeanOrdo;
	features["C70"] = dSTATSRPTMaxOrdo;
	features["C71"] = dSTATSRPTMeanQ1;
	features["C72"] = dSTATSRPTMaxQ1;
	features["C73"] = dSTATSRPTMeanQ2;
	features["C74"] = dSTATSRPTMaxQ2;
	features["C75"] = dSTATSRPTMeanQ3;
	features["C76"] = dSTATSRPTMaxQ3;
	features["C77"] = dSTATSRPTMeanQ4;
	features["C78"] = dSTATSRPTMaxQ4;
	features["C79"] = dSTATSRPTMeanQ5;
	features["C80"] = dSTATSRPTMaxQ5;
	// Writing basic statistics
	features["Size"] = uiSTATSize;
	features["R"] = double(uiSTATR)/double(uiSTATSize);
	features["H"] = double(uiSTATH)/double(uiSTATSize);
	//cout << uiSTATH << "-" << features["H"];
	/*for (map<string, double>::iterator iter = features.begin(); iter != features.end(); ++iter)
	{
		cout << iter->first << "," << iter->second << endl;
	}
	system("pause");*/
	return features;
}

/**
 * Compute Completion Times on M1 and M2, and the sum of processing times on both machines after the window
 */
void CStatistics::STATComputeFam1()
{
	unsigned int uiBoucle;

	uiSTATCm1 = 0;
	uiSTATCm2 = 0;
	uiSTATP1 = 0;
	uiSTATP2 = 0;

	for (uiBoucle = 0; uiBoucle < uiSTATR; uiBoucle++)
	{
		uiSTATCm1 += N[(*pSTATSeq)[uiBoucle]].ptime[0];
		if (uiSTATCm2 > uiSTATCm1)
			uiSTATCm2 += N[(*pSTATSeq)[uiBoucle]].ptime[1];
		else uiSTATCm2 = uiSTATCm1 + N[(*pSTATSeq)[uiBoucle]].ptime[1];
	}
	for (uiBoucle = uiSTATR + uiSTATH; uiBoucle < uiSTATSize; uiBoucle++)
	{
		uiSTATP1 += N[(*pSTATSeq)[uiBoucle]].ptime[0];
		uiSTATP2 += N[(*pSTATSeq)[uiBoucle]].ptime[1];
	}
}

/**
 * Compute Quantiles of size 3 and 10 (SPT2 order)
 */
void CStatistics::STATComputeFam2and7()
{
	unsigned int uiLoop, uiNbSPT2;
	vector<double> x;
	vector<double> fx;
	double dQ1, dQ2, dQ3, dQ4, dQ5, dQ6, dQ7, dQ8, dQ9;
	dQ1 = dQ2 = dQ3 = dQ4 = dQ5 = dQ6 = dQ7 = dQ8 = dQ9 = 0;
	double min, max, sd;
	min = max = sd = 0;

	for (uiLoop = 0; uiLoop < uiSTATH + 1; uiLoop++)
	{
		x.push_back(uiLoop);
		fx.push_back(0);
	}
	uiNbSPT2 = 0;
	for (uiLoop = uiSTATR; uiLoop < uiSTATR + uiSTATH - 1; uiLoop++)
	{
		if (N[(*pSTATSeq)[uiLoop]].ptime[1] < N[(*pSTATSeq)[uiLoop + 1]].ptime[1]) uiNbSPT2++;
		else
		{
			if (uiNbSPT2 == 0) fx[0]++;
			else fx[uiNbSPT2 + 1]++;
			uiNbSPT2 = 0;
		}
	}
	STATComputeQuantile3(x, fx, &dQ1, &dQ2);
	dSTATQ31 = dQ1 / uiSTATH;
	dSTATQ32 = dQ2 / uiSTATH;
	STATComputeQuantile10(x, fx, &dQ1, &dQ2, &dQ3, &dQ4, &dQ5, &dQ6, &dQ7, &dQ8, &dQ9);
	dSTATQ101 = dQ1 / uiSTATH;
	dSTATQ102 = dQ2 / uiSTATH;
	dSTATQ103 = dQ3 / uiSTATH;
	dSTATQ104 = dQ4 / uiSTATH;
	dSTATQ105 = dQ5 / uiSTATH;
	dSTATQ106 = dQ6 / uiSTATH;
	dSTATQ107 = dQ7 / uiSTATH;
	dSTATQ108 = dQ8 / uiSTATH;
	dSTATQ109 = dQ9 / uiSTATH;
	STATComputeMinMaxSD(x, &min, &max, &sd);
	dSTATmin = min / uiSTATH;
	dSTATmax = max / uiSTATH;
	dSTATsd = sd / uiSTATH;
}

/**
 * Compute Quantiles of size 3 and 10 (Idle time order)
 */
void CStatistics::STATComputeFam3and8()
{
	unsigned int uiLoop, uiC1 = 0, uiC2 = 0;
	vector<double> x;
	vector<double> fx;
	double dQ1, dQ2, dQ3, dQ4, dQ5, dQ6, dQ7, dQ8, dQ9, dIdleTime, dpmax;
	dQ1 = dQ2 = dQ3 = dQ4 = dQ5 = dQ6 = dQ7 = dQ8 = dQ9 = 0;
	double min, max, sd;
	min = max = sd = 0;

	for (uiLoop = 0; uiLoop < uiSTATR; uiLoop++)
	{ // Computation of Completion times on M1 and M2 before the window of positions
		uiC1 += N[(*pSTATSeq)[uiLoop]].ptime[0];
		if (uiC1 > uiC2)
			uiC2 = uiC1 + N[(*pSTATSeq)[uiLoop]].ptime[1];
		else uiC2 += N[(*pSTATSeq)[uiLoop]].ptime[1];
	}
	dpmax = 0;
	for (uiLoop = uiSTATR; uiLoop < uiSTATR + uiSTATH; uiLoop++) // We compute pmax
		if (N[(*pSTATSeq)[uiLoop]].ptime[1] > dpmax)
			dpmax = N[(*pSTATSeq)[uiLoop]].ptime[1];
	// We now compute the idle times
	for (uiLoop = uiSTATR; uiLoop < uiSTATR + uiSTATH; uiLoop++)
	{
		uiC1 += N[(*pSTATSeq)[uiLoop]].ptime[0];
		if (uiC1 >= uiC2)
		{
			dIdleTime = uiC1 - uiC2;
			x.push_back(dIdleTime / dpmax);
			fx.push_back(1);
		}
		if (uiC1 > uiC2)
			uiC2 = uiC1 + N[(*pSTATSeq)[uiLoop]].ptime[1];
		else uiC2 += N[(*pSTATSeq)[uiLoop]].ptime[1];
	}
	if (x.size() > 0)
	{
		sort(x.begin(), x.end());
		STATComputeQuantile3(x, fx, &dQ1, &dQ2);
		STATComputeQuantile10(x, fx, &dQ1, &dQ2, &dQ3, &dQ4, &dQ5, &dQ6, &dQ7, &dQ8, &dQ9);
		STATComputeMinMaxSD(x, &min, &max, &sd);
	}
	dSTATitQ31 = dQ1;
	dSTATitQ32 = dQ2;
	dSTATitQ101 = dQ1;
	dSTATitQ102 = dQ2;
	dSTATitQ103 = dQ3;
	dSTATitQ104 = dQ4;
	dSTATitQ105 = dQ5;
	dSTATitQ106 = dQ6;
	dSTATitQ107 = dQ7;
	dSTATitQ108 = dQ8;
	dSTATitQ109 = dQ9;
	dSTATitmin = min;
	dSTATitmax = max;
	dSTATitsd = sd;
}

/**
 * Compute Quantiles of size 3 and 10 (processing times ratio order)
 */
void CStatistics::STATComputeFam4and9()
{
	unsigned int uiLoop, uiLoop2, uiLoop3, uiNbRatio = 0;
	bool bAdded;
	vector<double> x;
	vector<double> fx;
	double dQ1, dQ2, dQ3, dQ4, dQ5, dQ6, dQ7, dQ8, dQ9, dratio;
	double min, max, sd;
	min = max = sd = 0;

	// We now compute the ratios
	for (uiLoop = uiSTATR; uiLoop < uiSTATR + uiSTATH; uiLoop++)
	{
		dratio = (double)N[(*pSTATSeq)[uiLoop]].ptime[0] / (double)N[(*pSTATSeq)[uiLoop]].ptime[1];
		bAdded = false;
		for (uiLoop2 = 0; uiLoop2 < uiNbRatio && !bAdded; uiLoop2++)
		{
			if (dratio >= x[uiLoop2] - 0.00001 && dratio <= x[uiLoop2] + 0.00001)
			{
				fx[uiLoop2]++;
				bAdded = true;
			}
			else if (dratio < x[uiLoop2] - 0.00001)
			{ // Insertion at position uiLoop2
				x.push_back(0);
				fx.push_back(0);
				for (uiLoop3 = uiNbRatio; uiLoop3 > uiLoop2; uiLoop3--)
				{
					x[uiLoop3] = x[uiLoop3 - 1];
					fx[uiLoop3] = fx[uiLoop3 - 1];
				}
				x[uiLoop2] = dratio;
				fx[uiLoop2] = 1;
				uiNbRatio++;
				bAdded = true;
			}
		}
		if (!bAdded)
		{
			x.push_back(dratio);
			fx.push_back(1);
			uiNbRatio++;
		}
	}

	STATComputeQuantile3(x, fx, &dQ1, &dQ2);
	dSTATprQ31 = dQ1;
	dSTATprQ32 = dQ2;
	STATComputeQuantile10(x, fx, &dQ1, &dQ2, &dQ3, &dQ4, &dQ5, &dQ6, &dQ7, &dQ8, &dQ9);
	dSTATprQ101 = dQ1;
	dSTATprQ102 = dQ2;
	dSTATprQ103 = dQ3;
	dSTATprQ104 = dQ4;
	dSTATprQ105 = dQ5;
	dSTATprQ106 = dQ6;
	dSTATprQ107 = dQ7;
	dSTATprQ108 = dQ8;
	dSTATprQ109 = dQ9;
	STATComputeMinMaxSD(x, &min, &max, &sd);
	dSTATprmin = min;
	dSTATprmax = max;
	dSTATprsd = sd;
}

/**
 * Compute Quantiles of size 3 and 10 (Idle time order, right of the window)
 */
void CStatistics::STATComputeFam6and10()
{
	unsigned int uiLoop, uiC1 = 0, uiC2 = 0, uiNbit = 0;
	vector<double> x;
	vector<double> fx;
	double dQ1, dQ2, dQ3, dQ4, dQ5, dQ6, dQ7, dQ8, dQ9, dIdleTime, dpmax;
	dQ1 = dQ2 = dQ3 = dQ4 = dQ5 = dQ6 = dQ7 = dQ8 = dQ9 = 0;
	double min, max, sd;
	min = max = sd = 0;

	for (uiLoop = 0; uiLoop < uiSTATR + uiSTATH; uiLoop++)
	{ // Computation of Completion times on M1 and M2 before the window of positions
		uiC1 += N[(*pSTATSeq)[uiLoop]].ptime[0];
		if (uiC1 > uiC2)
			uiC2 = uiC1 + N[(*pSTATSeq)[uiLoop]].ptime[1];
		else uiC2 += N[(*pSTATSeq)[uiLoop]].ptime[1];
	}
	dpmax = 0;
	for (uiLoop = uiSTATR + uiSTATH; uiLoop < uiSTATSize; uiLoop++) // We compute pmax
		if (N[(*pSTATSeq)[uiLoop]].ptime[1] > dpmax)
			dpmax = N[(*pSTATSeq)[uiLoop]].ptime[1];
	// We now compute the idle times
	for (uiLoop = uiSTATR + uiSTATH; uiLoop < uiSTATSize; uiLoop++)
	{
		uiC1 += N[(*pSTATSeq)[uiLoop]].ptime[0];
		if (uiC1 >= uiC2)
		{
			dIdleTime = uiC1 - uiC2;
			x.push_back(dIdleTime / dpmax);
			fx.push_back(1);
			uiNbit++;
		}
		if (uiC1 > uiC2)
			uiC2 = uiC1 + N[(*pSTATSeq)[uiLoop]].ptime[1];
		else uiC2 += N[(*pSTATSeq)[uiLoop]].ptime[1];
	}
	if (x.size() > 0)
	{
		sort(x.begin(), x.end());
		STATComputeQuantile3(x, fx, &dQ1, &dQ2);
		STATComputeQuantile10(x, fx, &dQ1, &dQ2, &dQ3, &dQ4, &dQ5, &dQ6, &dQ7, &dQ8, &dQ9);
		STATComputeMinMaxSD(x, &min, &max, &sd);
	}
	dSTATitRQ31 = dQ1;
	dSTATitRQ32 = dQ2;
	dSTATitRQ101 = dQ1;
	dSTATitRQ102 = dQ2;
	dSTATitRQ103 = dQ3;
	dSTATitRQ104 = dQ4;
	dSTATitRQ105 = dQ5;
	dSTATitRQ106 = dQ6;
	dSTATitRQ107 = dQ7;
	dSTATitRQ108 = dQ8;
	dSTATitRQ109 = dQ9;
	dSTATitRmin = min;
	dSTATitRmax = max;
	dSTATitRsd = sd;
}

/**
 * Compute Family 12 statistics
 */
void CStatistics::STATComputeFam12()
{
	unsigned int uiLoop, uiCompTimeLoop, uiCompTimeSeq, uiCompTimeSRPT, uiLastJobPosition;
	unsigned int uiC1, uiC2;

	vector<double> SRPTsubstract;
	vector<unsigned int> v1;
	vector<unsigned int> v2;
	STATSRPT(&v1, &v2);

	//Compute for each job completion time in the sequence and in SRPT scheduling
	for (uiLoop = 0; uiLoop < uiSTATSize; uiLoop++)
	{
		uiCompTimeSeq = 0;
		uiCompTimeSRPT = 0;
		uiCompTimeLoop = 0;
		uiC1 = uiC2 = 0;

		while ((*pSTATSeq)[uiCompTimeLoop] != uiLoop)
		{ // Computation of Completion times on M1 and M2
			uiC1 += N[(*pSTATSeq)[uiCompTimeLoop]].ptime[0];
			if (uiC1 > uiC2)
				uiC2 = uiC1 + N[(*pSTATSeq)[uiCompTimeLoop]].ptime[1];
			else uiC2 += N[(*pSTATSeq)[uiCompTimeLoop]].ptime[1];
			uiCompTimeLoop++;
		}
		uiC1 += N[(*pSTATSeq)[uiCompTimeLoop]].ptime[0];
		if (uiC1 > uiC2)
			uiC2 = uiC1 + N[(*pSTATSeq)[uiCompTimeLoop]].ptime[1];
		else uiC2 += N[(*pSTATSeq)[uiCompTimeLoop]].ptime[1];

		uiCompTimeSeq = uiC2;

		//SRPT completion time
		uiLastJobPosition = v1.size() - 1;
		while (v1[uiLastJobPosition] != uiLoop)
		{
			uiLastJobPosition--;
		}

		for (uiCompTimeLoop = 0; uiCompTimeLoop <= uiLastJobPosition; uiCompTimeLoop++) {
			uiCompTimeSRPT = uiCompTimeSRPT + v2[uiCompTimeLoop];
		}

		SRPTsubstract.push_back((int)uiCompTimeSeq - (int)uiCompTimeSRPT);
	}

	// SRPT in window
	vector<double> vSRPTtemp;
	for (uiLoop = uiSTATR; uiLoop < uiSTATR + uiSTATH; uiLoop++)
	{
		vSRPTtemp.push_back(SRPTsubstract[(*pSTATSeq)[uiLoop]]);
	}
	sort(vSRPTtemp.begin(), vSRPTtemp.end());
	dSTATSRPTMaxWindow = vSRPTtemp.back() / (double)uiSTATH;
	dSTATSRPTMeanWindow = STATComputeMean(vSRPTtemp) / (double)uiSTATH;

	// SRPT on entire sequence
	vSRPTtemp.clear();
	for (uiLoop = 0; uiLoop < uiSTATSize; uiLoop++)
	{
		vSRPTtemp.push_back(SRPTsubstract[(*pSTATSeq)[uiLoop]]);
	}
	sort(vSRPTtemp.begin(), vSRPTtemp.end());
	dSTATSRPTMaxOrdo = vSRPTtemp.back() / (double)uiSTATSize;
	dSTATSRPTMeanOrdo = STATComputeMean(vSRPTtemp) / (double)uiSTATSize;

	// SRPT on 5 quantiles
	vector<double> vSRPTQ1;
	vector<double> vSRPTQ2;
	vector<double> vSRPTQ3;
	vector<double> vSRPTQ4;
	vector<double> vSRPTQ5;
	for (uiLoop = 0; uiLoop < uiSTATSize; uiLoop++)
	{
		if (uiLoop < uiSTATSize / double(5)) {
			vSRPTQ1.push_back(SRPTsubstract[(*pSTATSeq)[uiLoop]]);
		}
		else if (uiLoop < 2 * uiSTATSize / double(5)) {
			vSRPTQ2.push_back(SRPTsubstract[(*pSTATSeq)[uiLoop]]);
		}
		else if (uiLoop < 3 * uiSTATSize / double(5)) {
			vSRPTQ3.push_back(SRPTsubstract[(*pSTATSeq)[uiLoop]]);
		}
		else if (uiLoop < 4 * uiSTATSize / double(5)) {
			vSRPTQ4.push_back(SRPTsubstract[(*pSTATSeq)[uiLoop]]);
		}
		else if (uiLoop < uiSTATSize) {
			vSRPTQ5.push_back(SRPTsubstract[(*pSTATSeq)[uiLoop]]);
		}
	}
	sort(vSRPTQ1.begin(), vSRPTQ1.end());
	sort(vSRPTQ2.begin(), vSRPTQ2.end());
	sort(vSRPTQ3.begin(), vSRPTQ3.end());
	sort(vSRPTQ4.begin(), vSRPTQ4.end());
	sort(vSRPTQ5.begin(), vSRPTQ5.end());
	dSTATSRPTMaxQ1 = vSRPTQ1.back() / ((double)uiSTATSize / 5.0);
	dSTATSRPTMeanQ1 = STATComputeMean(vSRPTQ1) / ((double)uiSTATSize / 5.0);
	dSTATSRPTMaxQ2 = vSRPTQ2.back() / ((double)uiSTATSize / 5.0);
	dSTATSRPTMeanQ2 = STATComputeMean(vSRPTQ2) / ((double)uiSTATSize / 5.0);
	dSTATSRPTMaxQ3 = vSRPTQ3.back() / ((double)uiSTATSize / 5.0);
	dSTATSRPTMeanQ3 = STATComputeMean(vSRPTQ3) / ((double)uiSTATSize / 5.0);
	dSTATSRPTMaxQ4 = vSRPTQ4.back() / ((double)uiSTATSize / 5.0);
	dSTATSRPTMeanQ4 = STATComputeMean(vSRPTQ4) / ((double)uiSTATSize / 5.0);
	dSTATSRPTMaxQ5 = vSRPTQ5.back() / ((double)uiSTATSize / 5.0);
	dSTATSRPTMeanQ5 = STATComputeMean(vSRPTQ5) / ((double)uiSTATSize / 5.0);
}

/**
 * Computes the SRPT schedule of the jobs contained in the data structure N defined in CData.h
 *
 * \return pSRPTSeq the sequence of jobs in SRPT order
 * \return pSRPTProc the sequence of processing times associated to the sequence of jobs (due to preemption)
 */
void CStatistics::STATSRPT(vector<unsigned int> * pSRPTSeq, vector <unsigned int> * pSRPTProc)
{
	unsigned int uiLoop, uiCmax, uiJReady, uiPReady, uiJpre;
	vector<CElement> ListRj, ListPj;
	vector<unsigned int> Sched;

	for (uiLoop = 0; uiLoop < N.size(); uiLoop++)
		ListRj.push_back(CElement((unsigned int)N[uiLoop].id, (unsigned int)N[uiLoop].ptime[0]));
	sort(ListRj.begin(), ListRj.end());

	uiCmax = 0;
	while (ListRj.size() > 0)
	{ // Some jobs are not yet available
		if (ListPj.size() == 0)
		{ // No availabe job to schedule : we directly add the first available one
			uiCmax = ListRj[0].uiELEValue;
			ListRj[0].uiELEValue = N[ListRj[0].uiELEJob].ptime[1];
			ListPj.push_back(ListRj[0]);
			ListRj.erase(ListRj.begin());
			sort(ListPj.begin(), ListPj.end());
		}
		else
		{ // We have one available job to schedule

			// We add some jobs that may have become ready
			while (ListRj.size() > 0 && ListRj[0].uiELEValue <= uiCmax)
			{
				ListRj[0].uiELEValue = N[ListRj[0].uiELEJob].ptime[1];
				ListPj.push_back(ListRj[0]);
				ListRj.erase(ListRj.begin());
			}
			sort(ListPj.begin(), ListPj.end());
			// We now schedule the smallest available processing time job
			uiJReady = ListPj[0].uiELEJob;
			uiPReady = ListPj[0].uiELEValue;
			uiJpre = 0;
			while (uiJpre < ListRj.size() && ListRj[uiJpre].uiELEValue < uiCmax + uiPReady && (uiCmax + uiPReady <= (ListRj[uiJpre].uiELEValue + N[ListRj[uiJpre].uiELEJob].ptime[1])))
				uiJpre++;
			if (ListRj.size() == 0 || (uiCmax + uiPReady <= (ListRj[uiJpre].uiELEValue + N[ListRj[uiJpre].uiELEJob].ptime[1])))
			{ // We completely schedule the job
				(*pSRPTSeq).push_back(uiJReady);
				(*pSRPTProc).push_back(uiPReady);
				uiCmax += uiPReady;
				ListPj.erase(ListPj.begin());
				// We add some jobs that may have become ready
				while (ListRj.size() > 0 && ListRj[0].uiELEValue <= uiCmax)
				{
					ListRj[0].uiELEValue = N[ListRj[0].uiELEJob].ptime[1];
					ListPj.push_back(ListRj[0]);
					ListRj.erase(ListRj.begin());
				}
				sort(ListPj.begin(), ListPj.end());
			}
			else
			{ // We partially schedule the current job
				(*pSRPTSeq).push_back(uiJReady);
				(*pSRPTProc).push_back(ListRj[uiJpre].uiELEValue - uiCmax);
				ListPj[0].uiELEValue = uiCmax + uiPReady - ListRj[uiJpre].uiELEValue;
				uiCmax = ListRj[uiJpre].uiELEValue;
			}
		}
	}
	// It may remain some jobs in the ListPj of available jobs: they are sorted by SPT
	while (ListPj.size() > 0)
	{
		(*pSRPTSeq).push_back(ListPj[0].uiELEJob);
		(*pSRPTProc).push_back(ListPj[0].uiELEValue);
		ListPj.erase(ListPj.begin());
	}
}


/**
 * Instrumental method to compute quantiles of size 3
 *
 * \param x the list of elements
 * \param fx the number of times x appears
 * \return px1 and px2 contain the first and second quantile
 */
void CStatistics::STATComputeQuantile3(vector<double> x, vector<double> fx, double *px1, double *px2)
{
	unsigned int uiLoop;
	double dSum = 0, dPartialSum = 0;

	*px1 = *px2 = x[0];
	for (uiLoop = 0; uiLoop < x.size(); uiLoop++)
		dSum += fx[uiLoop];
	for (uiLoop = 0; uiLoop < x.size(); uiLoop++)
	{
		dPartialSum += fx[uiLoop];
		if (dPartialSum / dSum < (double)0.333333)
			*px1 = x[uiLoop];
		if (dPartialSum / dSum < (double)0.666666)
			*px2 = x[uiLoop];
	}
}


/**
 * Instrumental method to compute quantiles of size 10
 *
 * \param x the list of elements
 * \param fx the number of times x appears
 * \return px1, px2, px3, px4, px5, px6, px7, px8, px9 contain the 9 quantiles
 */
void CStatistics::STATComputeQuantile10(vector<double> x, vector<double> fx, double *px1, double *px2, double *px3, double *px4, double *px5, double *px6, double *px7, double *px8, double *px9)
{
	unsigned int uiLoop;
	double dSum = 0, dPartialSum = 0;

	*px1 = *px2 = *px3 = *px4 = *px5 = *px6 = *px7 = *px8 = *px9 = x[0];
	for (uiLoop = 0; uiLoop < x.size(); uiLoop++)
		dSum += fx[uiLoop];
	for (uiLoop = 0; uiLoop < x.size(); uiLoop++)
	{
		dPartialSum += fx[uiLoop];
		if (dPartialSum / dSum < (double)0.1)
			*px1 = x[uiLoop];
		if (dPartialSum / dSum < (double)0.2)
			*px2 = x[uiLoop];
		if (dPartialSum / dSum < (double)0.3)
			*px3 = x[uiLoop];
		if (dPartialSum / dSum < (double)0.4)
			*px4 = x[uiLoop];
		if (dPartialSum / dSum < (double)0.5)
			*px5 = x[uiLoop];
		if (dPartialSum / dSum < (double)0.6)
			*px6 = x[uiLoop];
		if (dPartialSum / dSum < (double)0.7)
			*px7 = x[uiLoop];
		if (dPartialSum / dSum < (double)0.8)
			*px8 = x[uiLoop];
		if (dPartialSum / dSum < (double)0.9)
			*px9 = x[uiLoop];
	}
}

/**
 * Compute min, max and standard deviation
 *
 * \param fx the vector
 * \return pmax, pin, psd contains the max, min and standard deviation
 */
void CStatistics::STATComputeMinMaxSD(vector<double> x, double *pmin, double *pmax, double *psd)
{
	// Min et max
	sort(x.begin(), x.end());
	*pmin = x.front();
	*pmax = x.back();

	// Standard deviation
	double dSum = 0, dAvg = 0, dSd = 0;
	for (unsigned int uiLoop = 0; uiLoop < x.size(); uiLoop++)
		dSum += x[uiLoop];
	dAvg = dSum / x.size();
	double dE = 0;
	double dInverse = 1.0 / static_cast<double>(x.size());
	for (unsigned int uiLoop = 0; uiLoop < x.size(); uiLoop++)
		dE += pow(static_cast<double>(x[uiLoop]) - dAvg, 2);
	*psd = sqrt(dInverse*dE);
}

/**
 * Compute mean of a vector
 *
 * \param x the vector
 * \return the mean of the elements in the vector
 */
double CStatistics::STATComputeMean(vector<double> x) {
	double dSum = 0;
	for (unsigned int uiLoop = 0; uiLoop < x.size(); uiLoop++) {
		dSum += x[uiLoop];
	}
	return dSum / static_cast<double>(x.size());
}

/**
 * Compute processing time stats on both machines in the window
 */
void CStatistics::STATComputeProcessingTime(int iMinPt1, int iMaxPt1, int iMinPt2, int iMaxPt2)
{
	unsigned int uiLoop = 0;
	vector<double> jobsMachine1, jobsMachine2;

	for (uiLoop = uiSTATR; uiLoop < uiSTATR + uiSTATH; uiLoop++) {
		jobsMachine1.push_back(N[(*pSTATSeq)[uiLoop]].ptime[0]);
		jobsMachine2.push_back(N[(*pSTATSeq)[uiLoop]].ptime[1]);
	}

	STATComputeMinMaxSD(jobsMachine1, &dSTATwindowPI1min, &dSTATwindowPI1max, &dSTATwindowPI1sd);
	STATComputeMinMaxSD(jobsMachine2, &dSTATwindowPI2min, &dSTATwindowPI2max, &dSTATwindowPI2sd);

	dSTATwindowPI1max = dSTATwindowPI1max / iMaxPt1;
	dSTATwindowPI2max = dSTATwindowPI2max / iMaxPt2;
	dSTATwindowPI1sd = dSTATwindowPI1sd / iMaxPt1;
	dSTATwindowPI2sd = dSTATwindowPI2sd / iMaxPt2;
	dSTATwindowPI1min = iMinPt1 / dSTATwindowPI1min;
	dSTATwindowPI2min = iMinPt2 / dSTATwindowPI2min;
}