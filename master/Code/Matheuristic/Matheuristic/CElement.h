/**
 * \brief Class for storing a job with a given processing time.
 */

class CElement
{
public:
	unsigned int uiELEJob; // the job id
	unsigned int uiELEValue; // the processing time value

	/**
	 * Constructs a CElement for a given job and a given processing time.
	 *
	 * \param uiJob the job id.
	 * \param uiValue the job processing time.
	 */
	CElement(unsigned int uiJob, unsigned uiValue) { uiELEJob = uiJob; uiELEValue = uiValue; }

	/**
	 * Operator < overload, used for sorting.
	 */
	friend bool operator<(const CElement & ELE1, const CElement &ELE2)
	{
		return (ELE1.uiELEValue < ELE2.uiELEValue ? true : false);
	}
};