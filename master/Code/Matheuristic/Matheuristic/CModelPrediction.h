#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <iterator>
#include <iostream>
#include <memory>
#include <math.h>

#define FDEEP_FLOAT_TYPE double
#include <fdeep/fdeep.hpp>
using namespace std;

/**
 * \brief Class for storing a prediction model.
 */
class CModelPrediction
{
private:
	map<string, double> MODELCoefficientsFeatures; // {key,value} structure for storing coefficients related to each feature.
	map<string, double> MODELMeansFeatures; // {key,value} structure for storing means related to each feature.
	map<string, double> MODELVariancesFeatures; // {key,value} structure for storing variances related to each feature.
	double MODELMean; // mean of deltas from the train database.
	double MODELVariance; // variance of deltas from the train database.
	bool useFDEEP; // bool if should use fdeep library.
	unique_ptr<fdeep::model> pModel; // wrapper model for storing logistique regresion and MLP model.
public:
	/**
	 * Constructs a CModelPrediction from a given path.
	 *
	 * \param modelPath the path to the model file.
	 * \param useFDEEP if use fdeep library.
	 */
	CModelPrediction(string modelPath, bool useFDEEP);

	/**
	 * Predicts the delta from a given list of features.
	 *
	 * \param modelPath {key,value} structure for storing computed features values.
	 * \return delta the predicted delta.
	 */
	double MODELPredict(map<string, double> features);

	/**
	 * Destructor.
	 */
	~CModelPrediction();
};

