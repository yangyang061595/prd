import re
import pandas


class PredictionModel:
    """
        Class for storing the prediction model.
    """

    def __init__(self):

        """Init an empty prediction model."""

        # Data
        self.features = None

        # Standard scaler containing mean and variance for each feature
        self.sc = None

        # Learning results from the lasso cross-validation method
        self.lassocv_model = None
        self.lassocv_alpha = None
        self.lassocv_mse = None
        self.lassocv_rsquared = None
        self.lassocv_coefs = None
        self.lassocv_predict_mse = None
        self.lassocv_predict_rsquared = None

    def export_model(self, file_name):

        """
        Export the prediction model to a file.

        Args:
            file_name: file name for the exported the model.
        """

        if self.features is not None:
            model_file = open(file_name, "w+")

            # Export coefficients, means and variances for each feature
            dataframe = pandas.DataFrame({'Variables': self.features, 'Coefficients': self.lassocv_coefs, 'Variances': self.sc.var_[:len(self.sc.var_) - 1], 'Moyennes': self.sc.mean_[:len(self.sc.mean_) - 1]})
            data_str = re.sub("\s\s+", " ", dataframe.to_string())
            model_file.write(data_str)

            # Export mean and variance of the criteria
            model_file.write("\n" + str(self.sc.mean_[-1]))
            model_file.write("\n" + str(self.sc.var_[-1]))

        else:
            raise Exception("This model is empty. Please run construct_and_benchmark() method before exporting it")
