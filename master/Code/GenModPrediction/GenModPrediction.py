import sys
import numpy
from LassoModelBuilder import LassoModelBuilder
from LassoModelOptimizer import LassoModelOptimizer
from LearningDataset import LearningDataset


ALPHAS = numpy.array([0.00001, 0.0001, 0.0002, 0.0003, 0.0004, 0.0005, 0.0006, 0.0007,
                      0.0008, 0.0009, 0.001, 0.01, 0.02, 0.025, 0.05, 0.1, 0.25, 0.5, 0.8, 1.0])


def main(argv):

    """Main function."""

    plot_boolean = True
    if argv[3] == "silent":
        plot_boolean = False

    # First the LearningDataset is initialized with train and test databases
    dataset = LearningDataset(argv[1], argv[2])

    # Some features can be removed so they are not used in the learning part
    dataset.remove_feature("C11")
    dataset.remove_feature("PerImp")
    dataset.remove_feature("Size")
    dataset.remove_feature("R")
    dataset.remove_feature("H")

    # Then model is built using Lasso regression
    builder = LassoModelBuilder(dataset, ALPHAS)
    builder.lasso_path()
    builder.construct_and_benchmark(plot_boolean)

    # At this time the builder contains the raw prediction model
    builder.prediction_model.export_model("model_raw.dat")

    # Finally the lasso model is optimized and then exported
    optimizer = LassoModelOptimizer(builder)
    modele = optimizer.keep_useful_features(plot_boolean)
    modele.export_model("model.dat")

    print("Model successfully created")


if __name__ == '__main__':

    if len(sys.argv) < 4:
        print("Please provide train database path, test database path, plot mode")
    else:
        main(sys.argv)
