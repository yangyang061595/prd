import pandas
import numpy
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import Lasso
from sklearn.linear_model import lasso_path
from sklearn.linear_model import LassoCV
from sklearn.metrics import mean_squared_error
from PredictionModel import PredictionModel


class LassoModelBuilder:
    """
        Class for learning a flowshop prediction model.
    """

    def __init__(self, dataset=None, alphas_array=None):

        """
        Init the model construction from a train dataset path, a test dataset path, and an array of alphas to use for lasso regression.

        Args:
            dataset: the data for learning the flowshop model.
            alphas_array: the array of alphas to use for lasso regression.
        """

        self.prediction_model = PredictionModel()

        if dataset is not None:
            self.dataset = dataset
            self.standard_scale_datasets()
        else:
            raise Exception("Learning dataset can't be empty")

        if alphas_array is not None:
            self.alphas = alphas_array
        else:
            raise Exception("Alphas array can't be empty")

    def display_shape_datasets(self):

        """Display the shape of the datasets."""

        print(self.dataset.data_train.shape)
        print(self.dataset.data_test.shape)

    def standard_scale_datasets(self):

        """Standardizes the features of the datasets."""

        self.prediction_model.sc = StandardScaler()
        self.dataset.data_train_standardized = self.prediction_model.sc.fit_transform(self.dataset.data_train)
        self.dataset.data_test_standardized = self.prediction_model.sc.transform(self.dataset.data_test)

    def plot_histogram_train_data(self):

        """Plot repartition of the training data."""

        plt.figure(1)
        plt.hist(self.dataset.data_train.to_numpy()[:, self.dataset.number_of_features - 1])
        plt.show()

    def lasso_path(self):

        """Compute lasso path of the model."""

        alpha_for_path, coefs_lasso, _ = lasso_path(self.dataset.data_train_standardized[:, :self.dataset.number_of_features - 1], self.dataset.data_train_standardized[:, self.dataset.number_of_features - 1], alphas=self.alphas)

        colors = cm.rainbow(numpy.linspace(0, 1, self.dataset.number_of_features - 1))

        for i in range(coefs_lasso.shape[0]):
            plt.plot(alpha_for_path, coefs_lasso[i, :], c=colors[i])

        plt.xlabel('Alpha')
        plt.ylabel('Coefficients')
        plt.title('Lasso path')
        plt.show()

        # Number of non zeroes coefs for each alpha
        not_zeroes = numpy.apply_along_axis(func1d=numpy.count_nonzero, arr=coefs_lasso, axis=0)

        plt.plot(alpha_for_path, not_zeroes)
        plt.xlabel('Alpha')
        plt.ylabel('Nb. de variables')
        plt.title('Nb. variables vs. Alpha')
        plt.show()

    def construct_and_benchmark(self, show_graph):

        """
        Constructs the model using cross validation on the train dataset and estimates its accuracy.

        Args:
            show_graph: true to show the graphs during learning, false for a silent learning.
        """

        self.prediction_model.lassocv_model = LassoCV(alphas=self.alphas, normalize=True, fit_intercept=False, random_state=0, cv=5)
        self.prediction_model.lassocv_model.fit(self.dataset.data_train_standardized[:, :self.dataset.number_of_features - 1], self.dataset.data_train_standardized[:, self.dataset.number_of_features - 1])

        # Cross-validation MSE mean for each alpha
        avg_mse_train_by_alpha = numpy.mean(self.prediction_model.lassocv_model.mse_path_, axis=1)
        mse_alphas_dataframe = pandas.DataFrame({'alpha': self.prediction_model.lassocv_model.alphas_, 'MSE': avg_mse_train_by_alpha})
        best_mse = mse_alphas_dataframe[mse_alphas_dataframe['alpha'] == self.prediction_model.lassocv_model.alpha_].MSE.item()

        predictions_standardized = self.prediction_model.lassocv_model.predict(self.dataset.data_test_standardized[:, :self.dataset.number_of_features - 1])
        self.predictions_destandardized = predictions_standardized * numpy.sqrt(self.prediction_model.sc.var_[-1]) + self.prediction_model.sc.mean_[-1]
        # ground_truth = self.dataset.data_test.iloc[:, self.dataset.number_of_features-1]

        # Predictions perfs
        self.prediction_model.features = self.dataset.data_train.columns[:self.dataset.number_of_features - 1]
        self.prediction_model.lassocv_alpha = self.prediction_model.lassocv_model.alpha_
        self.prediction_model.lassocv_mse = best_mse
        self.prediction_model.lassocv_coefs = self.prediction_model.lassocv_model.coef_
        self.prediction_model.lassocv_predict_mse = mean_squared_error(self.dataset.data_test_standardized[:, self.dataset.number_of_features - 1], predictions_standardized)
        self.prediction_model.lassocv_rsquared = self.prediction_model.lassocv_model.score(self.dataset.data_train_standardized[:, :self.dataset.number_of_features - 1], self.dataset.data_train_standardized[:, self.dataset.number_of_features - 1])
        self.prediction_model.lassocv_predict_rsquared = self.prediction_model.lassocv_model.score(self.dataset.data_test_standardized[:, :self.dataset.number_of_features - 1], self.dataset.data_test_standardized[:, self.dataset.number_of_features - 1])

        if show_graph:
            # Plot train MSE vs alpha
            plt.plot(self.prediction_model.lassocv_model.alphas_, avg_mse_train_by_alpha)
            plt.xlabel('Alpha')
            plt.ylabel('MSE')
            plt.title('MSE vs. Alpha')
            plt.show()

            # Plot R² train and test vs alpha
            rsquared_array = []
            rsquared_predict_array = []

            for alpha in reversed(self.alphas):
                lasso_model = Lasso(alpha=alpha).fit(self.dataset.data_train_standardized[:, :self.dataset.number_of_features - 1], self.dataset.data_train_standardized[:, self.dataset.number_of_features - 1])
                rsquared_array.append(lasso_model.score(self.dataset.data_train_standardized[:, :self.dataset.number_of_features - 1], self.dataset.data_train_standardized[:, self.dataset.number_of_features - 1]))
                rsquared_predict_array.append(lasso_model.score(self.dataset.data_test_standardized[:, :self.dataset.number_of_features - 1], self.dataset.data_test_standardized[:, self.dataset.number_of_features - 1]))

            plt.plot(self.prediction_model.lassocv_model.alphas_, rsquared_array, 'r')
            plt.plot(self.prediction_model.lassocv_model.alphas_, rsquared_predict_array, 'b')
            plt.xlabel('Alpha')
            plt.ylabel('R²')
            plt.title('R² train & test vs. Alpha')
            plt.show()
