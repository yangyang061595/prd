import sys
import numpy as np
from LearningDataset import LearningDataset
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, Activation, Conv1D, GlobalAveragePooling1D, MaxPooling1D, Dropout, BatchNormalization, Flatten
# For custom metrics
import keras
import itertools
# For a mean squared error regression problem
from keras.optimizers import Adam

from sklearn.metrics import confusion_matrix


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


def main(argv):
    """Main function."""

    plot_boolean = True
    if argv[3] == "silent":
        plot_boolean = False

    # First the LearningDataset is initialized with train and test databases
    dataset = LearningDataset(argv[1], argv[2])
    dataset.convert_delta()

    # Some features can be removed so they are not used in the learning part
    dataset.remove_feature("C11")
    dataset.remove_feature("PerImp")

    X_train = dataset.data_train.iloc[:, :dataset.number_of_features-1]
    Y_train = dataset.data_train.iloc[:, dataset.number_of_features-1]
    timesteps = dataset.number_of_features-1

    model = Sequential()
    model.add(Conv1D(32, 3, activation='relu', input_shape=(timesteps, 1)))
    model.add(BatchNormalization())
    model.add(Conv1D(32, kernel_size=5, strides=2,
              padding='same', activation='relu'))
    model.add(BatchNormalization())

    model.add(Dropout(0.2))

    model.add(Conv1D(64, 3, activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv1D(64, kernel_size=5, strides=2,
              padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.2))

    model.add(Conv1D(128, 3, activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv1D(128, kernel_size=5, strides=2,
              padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.2))

    model.add(Conv1D(256, 3, activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv1D(256, kernel_size=5, strides=2,
              padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.2))

    model.add(Conv1D(512, 3, padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Conv1D(512, kernel_size=5, strides=2,
              padding='same', activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.2))

    model.add(Flatten())
    model.add(Dense(32, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.2))
    model.add(Dense(1, activation='sigmoid'))

    adamfct = Adam(lr=0.0001)
    model.compile(optimizer=adamfct,
                  loss='binary_crossentropy', metrics=['accuracy'])

    print(model.summary())
    checkpoint = keras.callbacks.ModelCheckpoint(
        "./CNNModel.h5", monitor='val_loss', verbose=0, save_best_only=True, save_weights_only=False, mode='auto', period=1)
    callbacks_list = [checkpoint]

    X_train = X_train.values.reshape(
        X_train.shape[0], dataset.number_of_features-1, 1)
    # Y_train = Y_train.values.reshape(Y_train.shape[0], dataset.number_of_features-1, 1, 1)

    # Train the model, iterating on the data in batches of 64 samples
    history = model.fit(X_train, Y_train, epochs=100, shuffle=True,
                        validation_split=0.5, batch_size=64, callbacks=callbacks_list)

    print("Model successfully created")

    model = keras.models.load_model('CNNModel.h5')
    results = model.evaluate(X_train, Y_train, batch_size=Y_train.shape[0])
    print('Results on the train dataset : loss, acc:', results)

    X_test = dataset.data_test.iloc[:, :dataset.number_of_features-1]
    Y_test = dataset.data_test.iloc[:, dataset.number_of_features-1]

    X_test = X_test.values.reshape(
        X_test.shape[0], dataset.number_of_features-1, 1)
    results = model.evaluate(X_test, Y_test, batch_size=Y_test.shape[0])
    print('Results on the test dataset : loss, acc:', results)

    if plot_boolean:
        plt.plot(history.history['accuracy'][10:])
        plt.plot(history.history['val_accuracy'][10:])
        plt.title('model accuracy')
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.legend(['train', 'valid'], loc='upper left')
        plt.show()

        Ypred = model.predict(X_test)

        YpredBina = np.rint(Ypred)
        cnf_matrix = confusion_matrix(keras.utils.to_categorical(Y_test).argmax(
            axis=1), keras.utils.to_categorical(YpredBina).argmax(axis=1))
        np.set_printoptions(precision=2)

        # Plot non-normalized confusion matrix
        plt.figure()
        class_names = ("0", "1")
        plot_confusion_matrix(cnf_matrix, classes=class_names,
                              title='Confusion matrix, without normalization')

        # Plot normalized confusion matrix
        plt.figure()
        plot_confusion_matrix(cnf_matrix, classes=class_names, normalize=True,
                              title='Normalized confusion matrix')

        plt.show()


if __name__ == '__main__':

    if len(sys.argv) < 4:
        print("Please provide train database path, test database path, plot mode")
    else:
        main(sys.argv)
