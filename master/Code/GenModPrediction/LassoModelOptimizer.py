import copy
import pandas
import numpy
import matplotlib.pyplot as plt
from PredictionModel import PredictionModel


class LassoModelOptimizer:
    """
        Class for storing algorithms to optimize the Lasso model.

        Other algorithms for optimizing the lasso model can be implemented in this class.
    """

    def __init__(self, builder):

        """
        Init the optimizer with a given model builder.

        Args:
            builder: the builder containing the model to optimize.
        """

        if builder is not None:
            self.builder = builder
        else:
            raise Exception("A model builder is needed")

    def keep_useful_features(self, show_graph):

        """
        Find the list of features which constructs the best model by adding features one by one.

        Args:
            show_graph: true to show the graphs during optimization, false for a silent optimization.

        Returns:
            best_model: the optimized model
        """

        nom_var = self.builder.dataset.data_train.columns[:self.builder.dataset.number_of_features - 1]
        coefs_dataframe = pandas.DataFrame({'Variables': nom_var, 'Coefficients': self.builder.prediction_model.lassocv_coefs})
        coefs_dataframe = coefs_dataframe.reindex(coefs_dataframe.Coefficients.abs().sort_values(ascending=False).index)

        mse_array = []
        rsquared_array = []
        rsquared_predict_array = []

        builder_tmp = copy.deepcopy(self.builder)

        best_model = PredictionModel()
        best_model.lassocv_mse = 9999

        for variable in range(len(coefs_dataframe.index) - 1, 0, -1):
            builder_tmp.dataset.remove_feature(coefs_dataframe.iloc[variable].get('Variables'))
            builder_tmp.standard_scale_datasets()
            builder_tmp.construct_and_benchmark(False)
            mse_array.append(builder_tmp.prediction_model.lassocv_mse)
            rsquared_array.append(builder_tmp.prediction_model.lassocv_rsquared)
            rsquared_predict_array.append(builder_tmp.prediction_model.lassocv_predict_rsquared)
            if builder_tmp.prediction_model.lassocv_mse < best_model.lassocv_mse:
                best_model = copy.deepcopy(builder_tmp.prediction_model)

        if show_graph:
            # Plot train MSE vs number of variables
            plt.plot(numpy.arange(1, len(coefs_dataframe.index)), mse_array[::-1])
            plt.xlabel('Nb. de variables')
            plt.ylabel('MSE')
            plt.title('Meilleur MSE train vs. Nb. de variables')
            plt.show()

            # Plot R² train and test vs alpha
            plt.plot(numpy.arange(1, len(coefs_dataframe.index)), rsquared_array[::-1], 'r')
            plt.plot(numpy.arange(1, len(coefs_dataframe.index)), rsquared_predict_array[::-1], 'b')
            plt.xlabel('Nb. de variables')
            plt.ylabel('R²')
            plt.title('R² train & test vs. Nb. de variables')
            plt.show()

        return best_model
