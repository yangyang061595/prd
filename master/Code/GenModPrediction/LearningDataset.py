import pandas
import numpy as np

class LearningDataset:
    """
        Class for storing the train and test databases.
    """

    def __init__(self, path_dataset_train=None, path_dataset_test=None):

        """
        Init the model construction from a train dataset path and a test dataset path.

        Args:
            path_dataset_train: path to the train database file.
            path_dataset_train: path to the test database file.
        """

        if(path_dataset_train is not None and path_dataset_test is not None):
            # Data
            self.data_train = pandas.read_csv(path_dataset_train, sep=' ', engine='python')
            self.data_test = pandas.read_csv(path_dataset_test, sep=' ', engine='python')
            self.data_train_standardized = None
            self.data_test_standardized = None

            if self.data_train.columns.all() == self.data_test.columns.all():
                self.number_of_features = self.data_train.shape[1]
            else:
                raise Exception("Both datasets need to have the same headers.")

    def remove_feature(self, feature_string):

        """Remove a feature (i.e a column) from the datasets."""

        if any(feature_string in s for s in self.data_train.columns) and any(feature_string in s for s in self.data_test.columns):
            self.data_train = self.data_train.drop([feature_string], axis=1)
            self.data_test = self.data_test.drop([feature_string], axis=1)
            self.number_of_features = self.number_of_features - 1
        else:
            raise Exception("Can't remove feature " + feature_string + ". The feature has not been found in datasets.")
    
    def convert_delta(self):

        """Convert delat to a categorical variable isOpt (0 or 1)."""
     
        self.data_train['Delta'] = self.data_train.apply(lambda row: 1 if row['Delta']>0 else 0, axis=1)
        self.data_train = self.data_train.rename(columns={'Delta': 'isOpt'})

        self.data_test['Delta'] = self.data_test.apply(lambda row: 1 if row['Delta']>0 else 0, axis=1)
        self.data_test = self.data_test.rename(columns={'Delta': 'isOpt'})



       
