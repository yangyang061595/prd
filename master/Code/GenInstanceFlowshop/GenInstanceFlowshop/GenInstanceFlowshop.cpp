// GenInstanceFlowshop.cpp : Generates a flowshop instance with a given number of jobs and a given number of machines (processing times between 1 and 100)
//

#include <iostream>
#include <ctime>
#include <string> 

using namespace std;

int main(int argc, char *argv[])
{
	int res = EXIT_SUCCESS;

	try {
		if (argc < 4) {
			throw("Please provide number of jobs, number of machines and number of instances to generate");
		}

		int iNumberOfJobs = atoi(argv[1]);
		int iNumberOfMachines = atoi(argv[2]);
		int iNumberOfInstances = atoi(argv[3]);

		system("mkdir instances > NUL 2>&1");

		srand(time(NULL));

		FILE *fichier;
		unsigned int uiLoopJobs, uiLoopMachines, uiLoopInstances;

		for (uiLoopInstances = 0; uiLoopInstances < iNumberOfInstances; uiLoopInstances++)
		{
			string path = "instances/instance" + to_string(uiLoopInstances+1) + ".txt";

			fichier = fopen(path.c_str(), "wt+");

			for (uiLoopJobs = 0; uiLoopJobs < iNumberOfJobs; uiLoopJobs++)
			{
				fprintf(fichier, "%d", uiLoopJobs + 1);
				for (uiLoopMachines = 0; uiLoopMachines < iNumberOfMachines; uiLoopMachines++)
				{
					double dRandomPTime = (((double)rand() / (double)RAND_MAX) * 99) + 1; //Generates a random processing time between 1 and 100
					int iRandomPTime = round(dRandomPTime);
					fprintf(fichier, " %d", iRandomPTime);
				}
				fprintf(fichier, "\n");
			}

			fclose(fichier);
		}

		cout << "Successfully generated " << iNumberOfInstances << " instances with " << iNumberOfJobs << " jobs and " << iNumberOfMachines << " machines" << endl;
	}
	catch (const char *s) {
		cerr << s << endl;
		res = EXIT_FAILURE;
	}
	return res;
}