#ifndef CPOO
#define CPOO 3

#include "CData.h"
#include "Cstatistics.h"

/**
 * \brief Class for storing a pool of sequences.
 */
class CPool
{
 private:
	 vector<int> **pPOOSeq;    // List of sequences in the pool
	 CStatistics **pPOOStat;   // List statistics attached to each sequence
	 double *pdPOOObjValue;      // Objective function value of the stored sequences
	 unsigned int uiPOOSize;  // Number of sequences in the pool

 public:
	 /**
	  * Constructs a CPool.
	  */
	 CPool() { uiPOOSize = 0; }

	 /**
	  * Copy constructor.
	  */
	 CPool(CPool &);

	 /**
	  * Destructor.
	  */
	 ~CPool();

	 /**
	  * Operator = overload.
	  */
	 CPool & operator=(CPool &);

	 /**
	  * Return the number of sequences in the pool.
	  *
	  * \return uiPOOSize the size of the pool.
	  */
	 unsigned int POOGetSize() { return uiPOOSize; }

	 /**
	  * Return the objective function value of the worse (first) sequence stored in the pool.
	  * Requires at least one sequence is in the pool.
	  *
	  * \return the worst objective value of the pool.
	  */
	 double POOGetWorseValue() { return pdPOOObjValue[0]; }

	 /**
	  * The method below add a new sequence to the pool, keeping the ordering of decreasing dValue.
	  *
	  * \param Seq the sequence to add.
	  * \param STATSeq the features corresponding to the sequence.
	  * \param dValue objective value of the sequence.
	  */
	 void POOAdd(vector<int> &Seq, CStatistics & STATSeq, double dValue);
	 
	 /**
	  * The method below remove a sequence from the pool, at a given position.
	  *
	  * \param uiPos the position of the sequence to remove.
	  */
	 void POORemove(unsigned int uiPos);

	 /** The method below append to a given text file the information stored in the pool.
	  *
	  * \param sName the path of the file to store the pool. 
	  */
	 void POOAppendToFile(const char *sName);
};


#endif

