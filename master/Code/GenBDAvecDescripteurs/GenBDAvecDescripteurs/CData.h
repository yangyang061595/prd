#ifndef CDat
#define CDat 2

#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include<stdio.h>
#include<process.h>
#include <list>

/**
 * Functions and structures for storing jobs.
 */

using namespace std;

/**
 * \brief Structure for storing jobs.
 */
struct Job {
	int id;
	int ptime[2];
};

/**
 * Jobs vector for storing the instance.
 */
extern vector<Job> N;

/**
 * Reads the instance from a given filepath.
 *
 * \param fname the path to the instance file.
 * \param N the jobs vector to fill.
 */
void read_jobs(const char *fname, vector<Job>& N);


#endif
