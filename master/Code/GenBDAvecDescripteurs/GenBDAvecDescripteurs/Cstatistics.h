#ifndef CSTAT
#define CSTAT 1

#include "CData.h"
#include "CElement.h"
#include <map>
#include <string>

/**
 * \brief Class for computing features of a sequence.
 */
class CStatistics
{
private:

	// Sequence
	vector<int>* pSTATSeq;

	// Basic statistics attached to the current sequence
	unsigned int uiSTATSize; //Size of the sequence
	unsigned int uiSTATR;  // Starting position of the windows for each sequence
	unsigned int uiSTATH;  // Size of the windows for each sequence
	double dSTATPerImp;    // Percentage of improvement for each sequence when reopt in the windows
	double dSTATPerImpInit;// Percentage of improvement for each sequence when reopt in the windows (wrt init seq)
	double dSTATDelta; // (S - S{r,h})/(n - r + 1) criteria

	// Family 1 of characteristics
	unsigned int uiSTATCm1; // Completion time of M1 before the time interval
	unsigned int uiSTATCm2; // Completion time of M2 before the time interval
	unsigned int uiSTATP1; // Sum of processing times of jobs on M1 after the interval
	unsigned int uiSTATP2; // Sum of processing times of jobs on M2 after the interval

	// Family 2 of characteristics: quantile of size 3 wrt SPT2
	double dSTATQ31;  // First quantile of size 3
	double dSTATQ32;  // Second quantile of size 3

	// Family 3 of characteristics: quantile of size 3 wrt idle times
	double dSTATitQ31;  // First quantile of size 3
	double dSTATitQ32;  // Second quantile of size 3

	// Family 4 of characteristics: quantile of size 3 wrt processing times ratios
	double dSTATprQ31;  // First quantile of size 3
	double dSTATprQ32;  // Second quantile of size 3

	// Family 5 of characteristics: relaxation value / sum of completion times
	double dSTATrelax;

	// Family 6 of characteristics: quantile of size 3 wrt idle times / right of window
	double dSTATitRQ31;  // First quantile of size 3
	double dSTATitRQ32;  // Second quantile of size 3

	// Family 7 of characteristics: quantile of size 10 wrt SPT2
	double dSTATQ101;  // First quantile of size 10
	double dSTATQ102;  // Second quantile of size 10
	double dSTATQ103;  // Third quantile of size 10
	double dSTATQ104;  // 4th quantile of size 10
	double dSTATQ105;  // 5th quantile of size 10
	double dSTATQ106;  // 6th quantile of size 10
	double dSTATQ107;  // 7th quantile of size 10
	double dSTATQ108;  // 8th quantile of size 10
	double dSTATQ109;  // 9th quantile of size 10

	// Family 8 of characteristics: quantile of size 10 wrt idle times
	double dSTATitQ101;  // First quantile of size 10
	double dSTATitQ102;  // Second quantile of size 10
	double dSTATitQ103;  // Third quantile of size 10
	double dSTATitQ104;  // 4th quantile of size 10
	double dSTATitQ105;  // 5th quantile of size 10
	double dSTATitQ106;  // 6th quantile of size 10
	double dSTATitQ107;  // 7th quantile of size 10
	double dSTATitQ108;  // 8th quantile of size 10
	double dSTATitQ109;  // 9th quantile of size 10

	// Family 9 of characteristics: quantile of size 10 wrt processing times ratios
	double dSTATprQ101;  // First quantile of size 10
	double dSTATprQ102;  // Second quantile of size 10
	double dSTATprQ103;  // Third quantile of size 10
	double dSTATprQ104;  // 4th quantile of size 10
	double dSTATprQ105;  // 5th quantile of size 10
	double dSTATprQ106;  // 6th quantile of size 10
	double dSTATprQ107;  // 7th quantile of size 10
	double dSTATprQ108;  // 8th quantile of size 10
	double dSTATprQ109;  // 9th quantile of size 10

	// Family 10 of characteristics: quantile of size 10 wrt idle times / right of window
	double dSTATitRQ101;  // First quantile of size 10
	double dSTATitRQ102;  // Second quantile of size 10
	double dSTATitRQ103;  // Third quantile of size 10
	double dSTATitRQ104;  // 4th quantile of size 10
	double dSTATitRQ105;  // 5th quantile of size 10
	double dSTATitRQ106;  // 6th quantile of size 10
	double dSTATitRQ107;  // 7th quantile of size 10
	double dSTATitRQ108;  // 8th quantile of size 10
	double dSTATitRQ109;  // 9th quantile of size 10

	// Family 11 of characteristics
	// SPT2
	double dSTATmin;  // Min
	double dSTATmax;  // Max
	double dSTATsd;  // Standard deviation
	// Idle times
	double dSTATitmin;  // Min
	double dSTATitmax;  // Max
	double dSTATitsd;  // Standard deviation
	// Processing times ratios
	double dSTATprmin;  // Min
	double dSTATprmax;  // Max
	double dSTATprsd;  // Standard deviation
	// Idle times / right of window
	double dSTATitRmin;  // Min
	double dSTATitRmax;  // Max
	double dSTATitRsd;  // Standard deviation
	// Processing times in window on each machine
	double dSTATwindowPI1min;  // Min
	double dSTATwindowPI1max;  // Max
	double dSTATwindowPI1sd;  // Standard deviation
	double dSTATwindowPI2min;  // Min
	double dSTATwindowPI2max;  // Max
	double dSTATwindowPI2sd;  // Standard deviation

	// Family 12 of characteristics: C_S(i,2) - C_SRPT(i)
	double dSTATSRPTMaxWindow;  // Max C_S(i,2) - C_SRPT(i) in window
	double dSTATSRPTMeanWindow;  // Mean C_S(i,2) - C_SRPT(i) in window
	double dSTATSRPTMaxOrdo;  // Max C_S(i,2) - C_SRPT(i) in sequence
	double dSTATSRPTMeanOrdo;  // Mean C_S(i,2) - C_SRPT(i) in sequence
	double dSTATSRPTMaxQ1;  // Max C_S(i,2) - C_SRPT(i) in sequence quantile 1
	double dSTATSRPTMeanQ1;  // Mean C_S(i,2) - C_SRPT(i) in sequence quantile 1  
	double dSTATSRPTMaxQ2;  // Max C_S(i,2) - C_SRPT(i) in sequence quantile 2
	double dSTATSRPTMeanQ2;  // Mean C_S(i,2) - C_SRPT(i) in sequence quantile 2
	double dSTATSRPTMaxQ3;  // Max C_S(i,2) - C_SRPT(i) in sequence quantile 3
	double dSTATSRPTMeanQ3;  // Mean C_S(i,2) - C_SRPT(i) in sequence quantile 3
	double dSTATSRPTMaxQ4;  // Max C_S(i,2) - C_SRPT(i) in sequence quantile 4
	double dSTATSRPTMeanQ4;  // Mean C_S(i,2) - C_SRPT(i) in sequence quantile 4
	double dSTATSRPTMaxQ5;  // Max C_S(i,2) - C_SRPT(i) in sequence quantile 5
	double dSTATSRPTMeanQ5;  // Mean C_S(i,2) - C_SRPT(i) in sequence quantile 5



public:
	/** 
	 * Compute the features on a given input sequence.
	 * 
	 * \param pSeq pointeur to the sequence to evaluate.
	 * \param uiSize size of the sequence.
	 * \param uiR starting position of the window of positions.
	 * \param uiH length of the window of positions.
	 */
	void STATComputeStatistics(vector<int> * pSeq, unsigned int uiSize, unsigned int uiR, unsigned int uiH, double dPerImp, double dPerImpInit, double dRelaxationValue, double dObjectiveValue, double dDelta);

	/** 
	 * The method below output the statistics on a single line of an already opened text file
	 * Requires : fichier as already been opened as a text file (fopen method)
	 */
	void STATAppendToFile(FILE *Fichier);

	/**
	 * Returns the window position.
	 *
	 * \return R (the window position).
	 */
	unsigned int STATGetR() { return uiSTATR; }

	/**
	 * Returns the window size.
	 *
	 * \return H (the window size).
	 */
	unsigned int STATGetH() { return uiSTATH; }

	/**
	 * Returns the PerImp.
	 *
	 * \return PerImp.
	 */
	double STATGetPerImp() { return dSTATPerImp; }

	/**
	 * Returns the Delta.
	 *
	 * \return Delta.
	 */
	double STATGetDelta() { return dSTATDelta; }

	/**
	 * Returns the sequence size.
	 *
	 * \return size (the sequence size).
	 */
	unsigned int STATGetSize() { return uiSTATSize; }

private:
	/**
	 * Compute Completion Times on M1 and M2, and the sum of processing times on both machines after the window
	 */
	void STATComputeFam1();

	/**
	 * Compute Quantiles of size 3 and 10 (SPT2 order)
	 */
	void STATComputeFam2and7();

	/**
	 * Compute Quantiles of size 3 and 10 (Idle time order)
	 */
	void STATComputeFam3and8();

	/**
	 * Compute Quantiles of size 3 and 10 (processing times ratio order)
	 */
	void STATComputeFam4and9();

	/**
	 * Compute Quantiles of size 3 and 10 (Idle time order, right of the window)
	 */
	void STATComputeFam6and10();

	/**
	 * Compute Family 12 statistics
	 */
	void STATComputeFam12();

	/**
	 * Computes the SRPT schedule of the jobs contained in the data structure N defined in CData.h
	 *
	 * \return pSRPTSeq the sequence of jobs in SRPT order
	 * \return pSRPTProc the sequence of processing times associated to the sequence of jobs (due to preemption)
	 */
	void STATSRPT(vector<unsigned int> * pSRPTSeq, vector <unsigned int> * pSRPTProc);

	/**
	 * Instrumental method to compute quantiles of size 3
	 * 
	 * \param x the list of elements
	 * \param fx the number of times x appears
	 * \return px1 and px2 contain the first and second quantile
	 */
	void STATComputeQuantile3(vector<double> x, vector<double> fx, double *px1, double *px2);

	/**
	 * Instrumental method to compute quantiles of size 10
	 * 
	 * \param x the list of elements
	 * \param fx the number of times x appears
	 * \return px1, px2, px3, px4, px5, px6, px7, px8, px9 contain the 9 quantiles
	 */
	void STATComputeQuantile10(vector<double> x, vector<double> fx, double *px1, double *px2, double *px3, double *px4, double *px5, double *px6, double *px7, double *px8, double *px9);

	/**
	 * Compute min, max and standard deviation
	 *
	 * \param fx the vector
	 * \return pmax, pin, psd contains the max, min and standard deviation
	 */
	void STATComputeMinMaxSD(vector<double> fx, double *pmax, double *pmin, double *psd);

	/** 
	 * Compute mean of a vector
	 *
	 * \param x the vector
	 * \return the mean of the elements in the vector
	 */
	double STATComputeMean(vector<double> x);

	/**
	 * Compute processing time stats on both machines in the window
	 */
	void STATComputeProcessingTime(int iMinPt1, int iMaxPt1, int iMinPt2, int iMaxPt2);

};
#endif
