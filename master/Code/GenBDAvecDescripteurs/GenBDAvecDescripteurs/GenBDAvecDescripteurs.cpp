using namespace std;

#include <ilcplex/ilocplex.h>
ILOSTLBEGIN

#include <vector>
#include <list>
#include <algorithm>
#include <stdio.h>
#include <process.h>
#include "CData.h"
#include "Cstatistics.h"
#include "CPool.h"
#include <time.h>

#define MAX_JOBS 600

#define NB_BEST_SEL 5
#define NB_RAND_SEL 15

#define HEADER "C1 C2 C3 C4 C5 C6 C7 C8 C9 C10 C11 C121 C122 C13 C14 C15 C16 C17 C18 C19 C20 C21 C22 C23 C24 C25 C26 C27 C28 C29 C30 C31 C32 C33 C34 C35 C36 C37 C38 C39 C40 C41 C42 C43 C44 C45 C46 C47 C48 C49 C50 C51 C52 C53 C54 C55 C56 C57 C58 C59 C60 C61 C62 C63 C64 C65 C66 C67 C68 C69 C70 C71 C72 C73 C74 C75 C76 C77 C78 C79 C80 Size R H PerImp Delta\n"

double sp_time;
double avg_sp_time = 0;
double max_sp_time = 0;

int initial_obj;
int sp_cnt = 0;

/**
 * Gets sequence from CPlex result
 * \param cpx the CPlex instance
 * \param x the list of constraints
 * \param N the problem instance
 * \return S the sequence corresponding to the CPlex result
 */
void get_sequence(IloCplex cpx, IloIntVarArray x[], const vector<Job>& N, vector<int>& S)
{
	for (int i = 0; i < N.size(); ++i) {
		for (int j = 0; j < N.size(); ++j) {
			if (cpx.getValue(x[i][j]) > .5) {
				S[j] = N[i].id;
			}
		}
	}
}

/**
 * Generator for the database.
 *
 * \param cpx the CPlex instance
 * \param x the list of constraints
 * \param N the problem instance
 * \param S the sequence to optimize
 * \param bestobj the objective value returned by the RBS heuristic
 * \param model the predictor
 * \param timelim the time limit
 */
void search(IloCplex cpx, IloIntVarArray x[], vector<Job>& N, vector<int>& S, int&bestobj, string sDatabaseFileName)
{
	unsigned int wsize;
	unsigned int wstart;
	unsigned int uiIter = 0;
	unsigned int uiLoop;
	vector<int> bestS(N.size()), Sp(N.size());
	unsigned int uiBestr = 0, uiBesth = 0;
	int uiObjInit = bestobj, bestobjiter;
	double dPerImpInit, dPerImp;
	bool bImproved;
	double dProbaSel = 0, dProba, dDelta;

	// Data structures added for instrumenting the Matheuristic
	CStatistics STATCalculator;

	cout << "initial=" << bestobj << endl;
	for (uiLoop = 0; uiLoop < N.size(); uiLoop++)
		bestS[uiLoop] = S[uiLoop];

	for (wsize = 4; wsize <= 16; wsize++)
		for (wstart = 0; wstart < N.size() - wsize; wstart++)
			dProbaSel++;
	dProbaSel = (double)15.0 / dProbaSel;

	clock_t start_time = clock();

	bImproved = true;
	printf("Starting search\n");

	while (bImproved) {
		printf("Iteration %d\n", uiIter + 1);
		bImproved = false;
		bestobjiter = bestobj;
		// Data structures for pooling the best and random solutions
		CPool POOBest, POORandom;

		for (wsize = 4; wsize <= 16; wsize++)
		{
			printf("Size : %d\n", wsize);
			for (wstart = 0; wstart < N.size() - wsize; wstart++)
			{
				list<pair<int, int> > L;
				for (int i = 0; i < N.size(); i++) {
					if (i<wstart || i>wstart + wsize - 1) {
						x[S[i]][i].setLB(1);
						x[S[i]][i].setUB(1);
						L.push_back(pair<int, int>(S[i], i));
					}
				}
				clock_t sp_start = clock();

				double dRelaxationValue = 0; //Linear relaxation value
				cpx.setParam(IloCplex::Param::MIP::Limits::Nodes, 0); // Stop after root node.
				if (cpx.solve()) {
					dRelaxationValue = cpx.getBestObjValue();
					cpx.setParam(IloCplex::Param::MIP::Limits::Nodes, 210000000);
				}
				cpx.solve(); // Continue solve.

				sp_time = ((double)(clock() - sp_start) / CLOCKS_PER_SEC);
				avg_sp_time += sp_time;
				sp_cnt++;
				if (sp_time > max_sp_time) {
					max_sp_time = sp_time;
				}

				// Sp is the sequence resulting from the reoptimization
				// S is the incumbent sequence on which reoptimization is performed
				get_sequence(cpx, x, N, Sp);

				dPerImp = 100.0*(double)((bestobjiter - (int)cpx.getObjValue())) / (double)(bestobjiter);
				dPerImpInit = 100.0*(double)((uiObjInit - (int)cpx.getObjValue())) / (double)(uiObjInit);
				dDelta = (double)(bestobjiter - cpx.getObjValue()) / (double)(N.size() - wstart + 1);
				if (dPerImp < 0) {
					dPerImp = 0;
				}

				// We memorize if the optimized subsequence improves upon the best known one
				if ((double)cpx.getObjValue() < (double)(bestobj - 0.0001)) {
					bestobj = (int)(cpx.getObjValue());
					cout << "Improved to " << bestobj <<
						" at time " << ((double)(clock() - start_time)) / CLOCKS_PER_SEC << endl;
					for (uiLoop = 0; uiLoop < N.size(); uiLoop++)
						bestS[uiLoop] = Sp[uiLoop];
					uiBestr = wstart;
					uiBesth = wsize;
					bImproved = true;
				}

				// We now test if the current optimized solution is among the best 5 ones (we store the incumbent solution)
				if (POOBest.POOGetSize() < 5 || (POOBest.POOGetWorseValue() > (double)(cpx.getObjValue())))
				{
					STATCalculator.STATComputeStatistics(&S, N.size(), wstart, wsize, dPerImp, dPerImpInit, dRelaxationValue, (double)(cpx.getObjValue()), dDelta);
					POOBest.POOAdd(S, STATCalculator, (double)(cpx.getObjValue()));
					if (POOBest.POOGetSize() > 5) // We have 6 stored sequence, we remove the worse (first) one
						POOBest.POORemove(0);
				}
				else
				{ // We now test if the current optimized solution is selected among the 15 randomly selected ones
					dProba = (double)rand() / (double)RAND_MAX;
					if (POORandom.POOGetSize() < 15 || dProba <= dProbaSel)
					{
						STATCalculator.STATComputeStatistics(&S, N.size(), wstart, wsize, dPerImp, dPerImpInit, dRelaxationValue, (double)(cpx.getObjValue()), dDelta);
						POORandom.POOAdd(S, STATCalculator, (double)(cpx.getObjValue()));
						if (POORandom.POOGetSize() > 15) // We have 16 stored sequence, we remove the worse (first) one
							POORandom.POORemove(0);
					}
				}

				while (!L.empty()) {
					x[L.front().first][L.front().second].setLB(0);
					x[L.front().first][L.front().second].setUB(1);
					L.pop_front();
				}

			}
		}
		if (bImproved)
		{
			for (uiLoop = 0; uiLoop < N.size(); uiLoop++)
				S[uiLoop] = bestS[uiLoop];

			// We output the Pools in the database file
			POOBest.POOAppendToFile(sDatabaseFileName.c_str());
			POORandom.POOAppendToFile(sDatabaseFileName.c_str());
		}
		uiIter++;
	}
}

/**
 * Reads the sequence returned by the RBS heuristic
 *
 * \param fname the path to the file containg the sequence
 * \return S the sequence
 */
int read_sequence(const char *fname, vector<int>& S)
{
	ifstream ifs(fname);
	int obj;

	if (ifs.is_open()) {
		ifs >> obj;
		for (int i = 0; i < S.size(); ++i)
			ifs >> S[i];
	}
	else {
		char buf[128];
		snprintf(buf, 127, "read_sequence(): cannot open %s", fname);
		throw(buf);
	}
	return obj;
}

/**
 * Generate a random sequence
 *
 * \param fname the path to the file containg the sequence
 * \return S the sequence
 */
int read_random_sequence(vector<int>& S) {
	//vector<int> numbers = {16,11,19,2,12,9,10,18,6,1,5,15,8,7,4,3,13,17,14,0};
	vector<int> numbers;
	for (unsigned int uiBoucle = 0; uiBoucle < S.size(); uiBoucle++) {
		numbers.push_back(uiBoucle);
	}
	random_shuffle(numbers.begin(), numbers.end());

	for (unsigned int uiBoucle = 0; uiBoucle < S.size(); uiBoucle++) {
		S[uiBoucle] = numbers[uiBoucle];
		cout << S[uiBoucle] << "-";
	}
	cout << endl;

	unsigned int uiSTATCm1 = 0;
	unsigned int uiSTATCm2 = 0;
	unsigned int sommeCm2 = 0;
	for (unsigned int uiBoucle = 0; uiBoucle < S.size(); uiBoucle++)
	{
		uiSTATCm1 += N[S[uiBoucle]].ptime[0];
		if (uiSTATCm2 > uiSTATCm1)
			uiSTATCm2 += N[S[uiBoucle]].ptime[1];
		else uiSTATCm2 = uiSTATCm1 + N[S[uiBoucle]].ptime[1];
		sommeCm2 += uiSTATCm2;
	}
	cout << sommeCm2 << endl;
	return sommeCm2;
}

/**
 * Init the database files with headers
 */
void init_database_files() {
	if (FILE *file = fopen("DatabaseTrain.txt", "r")) {
		fclose(file);
	}
	else {
		FILE *file_write = fopen("DatabaseTrain.txt", "w");
		fprintf(file_write, HEADER);
		fclose(file_write);
	}
	if (FILE *file = fopen("DatabaseTest.txt", "r")) {
		fclose(file);
	}
	else {
		FILE *file_write = fopen("DatabaseTest.txt", "w");
		fprintf(file_write, HEADER);
		fclose(file_write);
	}
}

/**
 * Entry point of the program.
 */
int main(int argc, char *argv[])
{
	int res = EXIT_SUCCESS;
	try {
		if (argc < 6) {
			throw("Please provide instances folder name, instances min number, instances max number, cross-validation train percentage, RBS, and random seed (optional)");
		}

		if (argc == 7) {
			srand(atoi(argv[6]));
		}
		else {
			srand(time(NULL));
		}
		rand();

		int iLowerBoundInstances = atoi(argv[2]);
		int iUpperBoundInstances = atoi(argv[3]);
		unsigned int uiLoopInstances;
		double dCrossValidationTrainPercent = atof(argv[4]);
		int iUseRBS = atoi(argv[5]);

		cout << "Cross-validation repartition : " << dCrossValidationTrainPercent << endl;

		init_database_files();

		for (uiLoopInstances = iLowerBoundInstances; uiLoopInstances <= iUpperBoundInstances; uiLoopInstances++) {

			cout << "-------------------------------" << endl;
			cout << "Instance " << uiLoopInstances << " (up to " << iUpperBoundInstances << ")" << endl;
			cout << "-------------------------------" << endl;

			string sPath = argv[1];
			sPath = sPath + "/instance" + to_string(uiLoopInstances) + ".txt";

			read_jobs(sPath.c_str(), N);

			cout << N.size() << " jobs" << endl;

			IloEnv env;
			IloModel model(env);

			IloIntVarArray x[MAX_JOBS];
			for (int i = 0; i < N.size(); ++i) {
				char buf[128];
				x[i] = IloIntVarArray(env, N.size(), 0.0, 1.0);
				for (int j = 0; j < N.size(); ++j) {
					snprintf(buf, 127, "x(%d,%d)", i, j);
					x[i][j].setName(buf);
				}
			}

			IloNumVarArray Ctime[2];
			Ctime[0] = IloNumVarArray(env, N.size(), 0.0, IloInfinity);
			Ctime[1] = IloNumVarArray(env, N.size(), 0.0, IloInfinity);
			for (int i = 0; i < N.size(); ++i) {
				char buf[128];
				snprintf(buf, 127, "C(0,%d)", i);
				Ctime[0][i].setName(buf);
				snprintf(buf, 127, "C(1,%d)", i);
				Ctime[1][i].setName(buf);
			}

			model.add(IloMinimize(env, IloSum(Ctime[1])));

			for (int i = 0; i < N.size(); ++i) {
				IloExpr e(env);
				for (int j = 0; j < N.size(); ++j) {
					e += x[i][j];
				}
				model.add(e == 1.0);
				e.end();
			}

			for (int j = 0; j < N.size(); ++j) {
				IloExpr e(env);
				for (int i = 0; i < N.size(); ++i) {
					e += x[i][j];
				}
				model.add(e == 1.0);
			}

			for (int pos = 0; pos < N.size(); ++pos) {
				IloExpr tmp(env), tmp2(env), tmp3(env);
				tmp.clear();
				tmp2.clear();
				tmp3.clear();
				for (int i = 0; i < N.size(); ++i) {
					tmp += N[i].ptime[0] * x[i][pos];
					tmp2 += N[i].ptime[1] * x[i][pos];
					tmp3 += N[i].ptime[1] * x[i][pos];
				}
				if (!pos) {
					model.add(Ctime[0][0] >= tmp);
					model.add(Ctime[1][0] >= Ctime[0][0] + tmp2);
				}
				else {
					model.add(Ctime[0][pos] >= Ctime[0][pos - 1] + tmp);
					model.add(Ctime[1][pos] >= Ctime[1][pos - 1] + tmp2);
					model.add(Ctime[1][pos] >= Ctime[0][pos] + tmp3);
				}
			}

			IloCplex cpx(model);

			cpx.setParam(IloCplex::Param::ParamDisplay, 0);
			cpx.setParam(IloCplex::MIPDisplay, 0);
			cpx.setParam(IloCplex::EpGap, 0);
			cpx.setParam(IloCplex::Param::Threads, 3);

			vector<int> S(N.size());

			wchar_t Hname[] = L"RBS_FLOWSHOP.exe";
			_spawnl(P_WAIT, "RBS_FLOWSHOP.exe", "RBS_FLOWSHOP.exe", sPath.c_str(), NULL);
			char ubname[128];
			snprintf(ubname, 127, "ub.rep");

			int cur_best_obj;
			if (iUseRBS == 0) {
				cur_best_obj = read_random_sequence(S);
			}
			else {
				cur_best_obj = read_sequence(ubname, S);
			}
			initial_obj = cur_best_obj;

			clock_t t_start = clock();
			if (((double)rand() / (double)RAND_MAX) <= dCrossValidationTrainPercent) {
				search(cpx, x, N, S, cur_best_obj, "DatabaseTrain.txt");
			}
			else {
				search(cpx, x, N, S, cur_best_obj, "DatabaseTest.txt");
			}

			cout << "INITIALOBJ=" << initial_obj << endl;
			cout << "BESTOBJ=" << cur_best_obj << endl;
			cout << "Total time=" << ((double)(clock() - t_start) / CLOCKS_PER_SEC) << endl;
			cout << "Avg subproblem time=" << avg_sp_time / sp_cnt << endl;
			cout << "Max subproblem time=" << max_sp_time << endl;
		}
	}

	catch (const char *s) {
		cerr << s << endl;
		res = EXIT_FAILURE;
	}
	catch (IloException e) {
		cerr << e << endl;
		res = EXIT_FAILURE;
	}

	return res;
}
