#include "CPool.h"

/**
 * Copy constructor.
 */
CPool::CPool(CPool &POOSeq)
{
	unsigned int uiLoop;
	pPOOSeq = (vector<int>**)malloc(sizeof(vector<int>*)*POOSeq.uiPOOSize);
	pPOOStat = (CStatistics **)malloc(sizeof(CStatistics *)*POOSeq.uiPOOSize);
	pdPOOObjValue = (double *)malloc(sizeof(double)*POOSeq.uiPOOSize);
	for (uiLoop = 0; uiLoop < POOSeq.uiPOOSize; uiLoop++)
	{
		pPOOSeq[uiLoop] = new vector<int>;
		(*pPOOSeq[uiLoop]) = (*POOSeq.pPOOSeq[uiLoop]);
		pPOOStat[uiLoop] = new CStatistics;
		(*pPOOStat[uiLoop]) = (*POOSeq.pPOOStat[uiLoop]);
		pdPOOObjValue[uiLoop] = POOSeq.pdPOOObjValue[uiLoop];
	}
	uiPOOSize = POOSeq.uiPOOSize;
}

/**
 * Destructor.
 */
CPool::~CPool()
{
	unsigned int uiLoop;
	for (uiLoop = 0; uiLoop < uiPOOSize; uiLoop++)
	{
		delete pPOOSeq[uiLoop];
		delete pPOOStat[uiLoop];
	}
	free(pPOOSeq);
	free(pPOOStat);
	free(pdPOOObjValue);
}

/**
 * Operator = overload.
 */
CPool & CPool::operator=(CPool & POOSeq)
{
	unsigned int uiLoop;

	// We desallocate existing data structures
	for (uiLoop = 0; uiLoop < uiPOOSize; uiLoop++)
	{
		delete pPOOSeq[uiLoop];
		delete pPOOStat[uiLoop];
	}
	free(pPOOSeq);
	free(pPOOStat);
	free(pdPOOObjValue);

	// We assign 
	pPOOSeq = (vector<int>**)malloc(sizeof(vector<int>*)*POOSeq.uiPOOSize);
	pPOOStat = (CStatistics **)malloc(sizeof(CStatistics *)*POOSeq.uiPOOSize);
	pdPOOObjValue = (double *)malloc(sizeof(double)*POOSeq.uiPOOSize);
	for (uiLoop = 0; uiLoop < POOSeq.uiPOOSize; uiLoop++)
	{
		pPOOSeq[uiLoop] = new vector<int>;
		(*pPOOSeq[uiLoop]) = (*POOSeq.pPOOSeq[uiLoop]);
		pPOOStat[uiLoop] = new CStatistics;
		(*pPOOStat[uiLoop]) = (*POOSeq.pPOOStat[uiLoop]);
		pdPOOObjValue[uiLoop] = POOSeq.pdPOOObjValue[uiLoop];
	}
	uiPOOSize = POOSeq.uiPOOSize;

	return *this;
}

/**
 * The method below add a new sequence to the pool, keeping the ordering of decreasing dValue.
 *
 * \param Seq the sequence to add.
 * \param STATSeq the features corresponding to the sequence.
 * \param dValue objective value of the sequence.
 */
void CPool::POOAdd(vector<int> & Seq, CStatistics & STATSeq, double dValue)
{
	unsigned int uiLoop;
	// We resize arrays
	pPOOSeq = (vector<int>**)realloc(pPOOSeq,sizeof(vector<int>*)*(uiPOOSize+1));
	pPOOStat = (CStatistics **)realloc(pPOOStat,sizeof(CStatistics *)*(uiPOOSize + 1));
	pdPOOObjValue = (double *)realloc(pdPOOObjValue, sizeof(double)*(uiPOOSize + 1));
	
	// We determine the insertion position and right shift sequences
	for (uiLoop = uiPOOSize; uiLoop >= 1 && pdPOOObjValue[uiLoop-1] < dValue; uiLoop--)
	{
		pPOOSeq[uiLoop] = pPOOSeq[uiLoop - 1];
		pPOOStat[uiLoop] = pPOOStat[uiLoop - 1];
		pdPOOObjValue[uiLoop] = pdPOOObjValue[uiLoop - 1];
	}

	pPOOSeq[uiLoop] = new vector<int>;
	pPOOStat[uiLoop] = new CStatistics;
	pdPOOObjValue[uiLoop] = dValue;
	(*pPOOSeq[uiLoop]) = Seq;
	(*pPOOStat[uiLoop]) = STATSeq;
	uiPOOSize++;
}

/**
 * The method below remove a sequence from the pool, at a given position.
 *
 * uiPos the position of the sequence to remove.
 */
void CPool::POORemove(unsigned int uiPos)
{
	unsigned int uiLoop;

	delete pPOOSeq[uiPos];
	delete pPOOStat[uiPos];
	for (uiLoop = uiPos; uiLoop < uiPOOSize-1; uiLoop++)
	{
		pPOOSeq[uiLoop] = pPOOSeq[uiLoop + 1];
		pPOOStat[uiLoop] = pPOOStat[uiLoop + 1];
		pdPOOObjValue[uiLoop] = pdPOOObjValue[uiLoop + 1];

	}
	pPOOSeq = (vector<int>**)realloc(pPOOSeq, (uiPOOSize - 1) * sizeof(vector<int>*));
	pPOOStat = (CStatistics **)realloc(pPOOStat, (uiPOOSize - 1) * sizeof(CStatistics*));
	pdPOOObjValue = (double *)realloc(pdPOOObjValue, (uiPOOSize - 1) * sizeof(double));
	uiPOOSize--;
}

/** The method below append to a given text file the information stored in the pool.
 *
 * \param sName the path of the file to store the pool.
 */
void CPool::POOAppendToFile(const char * sName)
{
	FILE *fichier;
	unsigned int uiLoop,uiLoop2;

	fichier = fopen(sName, "at+");
		
	for (uiLoop = 0; uiLoop < uiPOOSize; uiLoop++)
	{
		//for (uiLoop2 = 0; uiLoop2 < (*pPOOSeq[uiLoop]).size(); uiLoop2++)
			//fprintf(fichier, "%d %d ", N[(*pPOOSeq[uiLoop])[uiLoop2]].ptime[0], N[(*pPOOSeq[uiLoop])[uiLoop2]].ptime[1]);
		// Next we store the characteristics computed on the sequence
		(*pPOOStat[uiLoop]).STATAppendToFile(fichier);	
		// We first write the basic information about the sequence
		fprintf(fichier, "%d %lf %lf %3.5lf %3.5lf", (*pPOOSeq[uiLoop]).size(), 
			(double)(*pPOOStat[uiLoop]).STATGetR()/(double)(*pPOOSeq[uiLoop]).size(), (double)(*pPOOStat[uiLoop]).STATGetH()/(double)(*pPOOSeq[uiLoop]).size(), (*pPOOStat[uiLoop]).STATGetPerImp(), (*pPOOStat[uiLoop]).STATGetDelta());
		fprintf(fichier, "\n");
	}
	
	fclose(fichier);
}
