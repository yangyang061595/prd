@echo off
setlocal EnableDelayedExpansion
set instancesfolder=%1
set nbinstances=%2
set /A nbthreads=%3-1
set /A nbinstancesperthread=%2/%3
set crossvalidationpercent=%5
set rbs=%4

for /l %%x in (0, 1, %nbthreads%) do (
   SET /A lowerbound=%%x*%nbinstancesperthread%+1
   IF %%x EQU %nbthreads% (
      SET upperbound=%nbinstances%
   ) ELSE (
      SET /A sumupperbound=%%x+1
      SET /A upperbound=!sumupperbound!*%nbinstancesperthread%
   )
   echo !lowerbound!
   echo !upperbound!
   START GenBDAvecDescripteurs.exe %instancesfolder% !lowerbound! !upperbound! !rbs! %crossvalidationpercent%
)
